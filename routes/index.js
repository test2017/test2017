
/*
* GET home page.
*/

var mongo = require('mongodb');
var async = require('async');

var mongoConn = require('../routes/connection');
var contentful = require('contentful-management'); 
var request = require('request');
var config = require('config');

var space_id =  process.env.CONTENTFUL_SPACE_ID;
var accessToken = process.env.CONTENTFUL_ACCESS_TOKEN;
var MAILCHIMP_API_KEY = process.env.MAILCHIMP_API_KEY;

var shortid = require('shortid');
var cron = require('node-cron');
var ObjectId = require('mongodb').ObjectID;
//var mcapi = require('mailchimp');
var mcapi = require('../node_modules/mailchimp-api/mailchimp');

//console.log(accessToken);

var client = contentful.createClient({
  // This is the access token for this space. Normally you get both ID and the token in the Contentful web app 
  accessToken: accessToken
})

exports.index = function(req, res)
{
	var hostaddress = "http://"+req.headers.host;

	if(req.session.userid)
	{
		var profileupdatestatus = 0;
		var userid = req.session.userid;
		mongoConn.storyColl.find({"_id": ObjectId(userid)}).toArray(function(err, document) 
		{
			if(!err)
			{		
				console.log(JSON.stringify(document));
				var profileupdatedb = document[0].profileupdate;
				if(profileupdatedb == true) profileupdatestatus = 1;
				else profileupdatestatus = 0;
				storylistlength = document[0].storylist.length;
				var profilepictureid = '';
				console.log("Storyview = "+JSON.stringify(document[0]));

				var pictureid = 0;
				if(profilepictureid != '')
				{
					pictureid = profilepictureid;
				}
				else
				{
					pictureid = 0;
				}

				var notificationcount = 0;
                if(document[0].notification != undefined)
                {
                    var notificationList = document[0].notification;
                    var i=0;
                    async.eachSeries(notificationList, function (notification, callback) 
                    {
                        if(notification.notificationstatus == '')i++;
                        callback();
                    });
                    notificationcount = i;
                }
                else
                {
                    notificationcount = 0;
                }

				var o_id = new mongo.ObjectID(pictureid);
				mongoConn.profilepictureColl.find({"_id":o_id}).toArray(function(err,profileimage) 
				{
					console.log(profileimage);
					var profileimageurl = '';
					if(profileimage.length >= 1)
					{
						var path = profileimage[0].path;
						console.log("-----------Storyview singleprofileimage - "+path);

						profileimageurl = hostaddress+"/"+path.split("public/")[1];
					}
					else
					{
						profileimageurl = hostaddress+"/img/pro-pic.png";
					}

					document[0].profilepicture = profileimageurl;
					console.log("Profilepicture = "+profileimageurl);
					console.log("formindexjs = "+profileupdatestatus);


	  				res.render('story', { title: 'Unipaper',
	  					sessiontrue: 1,
	  					profileupdate:0,
	  					profileupdatestatus:profileupdatestatus,
	  					storylistlength: storylistlength,
	  					writer:document[0] ,
	  					profilepictureurl:document[0].profilepicture,
	  					username:document[0].username,
	  					lastname:document[0].lastname,
	  					university:document[0].universityname,
	  					city:document[0].city,
	  					facebooklinks:document[0].facebooklinks,
	  					twitterlinks:document[0].twitterlinks,
	  					notificationcount:notificationcount	  					
	  				});

				});				
			}
		});
	}
	else
	{
  		res.render('index', { title: 'Unipaper' });
	}
			
};

//profile view functionality

exports.profileview  = function(req, res)
{
	var hostaddress = "http://"+req.headers.host;
	if(req.session.userid)
	{
		var userid = req.session.userid;
		mongoConn.storyColl.find({"_id": ObjectId(userid)}).toArray(function(err, document) 
		{
			if(err)
			{
				res.send(JSON.stringify({ "status": "failure","code" : 0,"msg":"Some server side error occured"}));
			}
			else
			{
				var username = "";
				var lastname = "";
				var universityname = "";
				var city = "";
				var email = "";
				var facebooklinks = "";
				var twitterlinks = "";
				var aboutyou = "";
				var interestentertainment = false;
				var interestsports = false;
				var interestnews = false;

				if(document.length >= 1)
				{
					username = document[0].username;
					lastname = document[0].lastname;
					universityname = document[0].universityname;
					city = document[0].city;
					email = document[0].email;
					facebooklinks = document[0].facebooklinks;
					twitterlinks = document[0].twitterlinks;
					aboutyou = document[0].aboutyou;
					profilepictureid = document[0].profilepicture[0];
					writerid = document[0].writerid;
					paypalemail = document[0].paypalemail;
					interestentertainment = document[0].interestentertainment;
					interestsports = document[0].interestsports;
					interestnews = document[0].interestnews;


					if(interestentertainment == true) interestentertainment = "true";
					if(interestsports == true) interestsports = "true";
					if(interestnews == true) interestnews = "true";

					var notificationcount = 0;
	                if(document[0].notification != undefined)
	                {
	                    var notificationList = document[0].notification;
	                    var i=0;
	                    async.eachSeries(notificationList, function (notification, callback) 
	                    {
	                        if(notification.notificationstatus == '')i++;
	                        callback();
	                    });
	                    notificationcount = i;
	                }
	                else
	                {
	                    notificationcount = 0;
	                }

					var pictureid = 0;
					if(profilepictureid != '')
					{
						pictureid = profilepictureid;
					}
					else
					{
						pictureid = 0;
					}

					var o_id = new mongo.ObjectID(pictureid);
					mongoConn.profilepictureColl.find({"_id":o_id}).toArray(function(err,profileimage) 
					{
						console.log("!!!!----Profileview  - profileimage = "+profileimage);
						var profileimageurl = '';
						if(profileimage.length >= 1)
						{
							//var path = profileimage[0].path;
							//console.log("-----------singleprofileimage - "+path);
							//profileimageurl = hostaddress+"/"+path.split("public/")[1];
							profileimageurl = profileimage[0].profilecontenfulimageurl;
						}
						else
						{
							profileimageurl = hostaddress+"/img/pro-pic.png";
						}

						console.log("!!!!----Profileview  - interestentertainment = "+interestentertainment);
						console.log("!!!!----Profileview  - interestsports = "+interestsports);
						console.log("!!!!----Profileview  - interestnews = "+interestnews);

						res.render('profileview', { title: 'Unipaper',
			  				sessiontrue:1,
			  				username:username,
			  				lastname:lastname,
			  				universityname:universityname,
			  				city:city,email:email,
			  				facebooklinks:facebooklinks,
			  				twitterlinks:twitterlinks,
			  				aboutyou:aboutyou,
			  				profileimage:profileimageurl,
			  				writerid:writerid,
			  				notificationcount:notificationcount,
			  				paypalemail:paypalemail,
			  				interestentertainment:interestentertainment,
			  				interestsports:interestsports,
			  				interestnews:interestnews
			  			});					
					});

				}
			}
		});
	}
	else
	{
	 	res.redirect('/');	
	}
};

exports.notification = function(req,res)
{
	getNotificationsCount(req,res,function(notificationcount)
	{
		res.render('notification', { 
			title: 'Unipaper',
			sessiontrue:1,	
			notificationcount:notificationcount,
		});	
	});		
};

exports.getNotifications = function(req,res)
{

	var userid = req.session.userid;
	if(req.session.userid)
	{
		mongoConn.storyColl.find({"_id": ObjectId(userid)}).toArray(function(err, document) 
		{
			if(err)
			{
				res.send(JSON.stringify({ "status": "failure","code" : 0,"msg":"Some server side error occured"}));
			}
			else
			{
				console.log("-----GetNotifications = "+JSON.stringify(document));
				if(document[0].notification != undefined)
				{
					console.log("-----GetNotifications = "+JSON.stringify(document[0].notification));

					res.send(document[0].notification);
				}
				else
				{
					console.log("-----GetNotifications empty");
					var json = [];
					res.send(json);
				}
			}
		});	
	}
	else
	{
		res.redirect('/');	
	}	
};	

exports.updateNotification = function(req,res)
{
	console.log("Request notification id = "+req.body.notifyid);
	var notifyid = req.body.notifyid;
	var userid = req.session.userid;
	if(req.session.userid)
	{
		mongoConn.storyColl.find({"_id": ObjectId(userid)}).toArray(function(err, document) 
		{
			if(err)
			{
				res.send(JSON.stringify({ "status": "failure","code" : 0,"msg":"Some server side error occured"}));
			}
			else
			{
				if(document[0].notification != undefined)
				{
					var notificationList = document[0].notification;
					var i = 0;
					async.eachSeries(notificationList, function (notification, callback) 
		   			{	
		   				console.log("----matching id = "+notification.notifyid+", "+notifyid)
		   				if(notification.notifyid == notifyid)
		   				{
		   					document[0].notification[i].notificationstatus = 'note-unread';

							mongoConn.storyColl.save(document[0],function(err,result) {});
		   				}
		   				i++;
		   				callback();
		   			});

					res.send(JSON.stringify({ "status": "success","code" : 1,"msg":"successfully status changed	"}));
				}				
			}
		});
	}
	else
	{
		res.redirect('/');	
	}
};


exports.deleteNotification = function(req,res)
{
	var notifyid = req.body.notifyid;
	var userid = req.session.userid;

	if(req.session.userid)
	{
		mongoConn.storyColl.find({"_id": ObjectId(userid)}).toArray(function(err, document) 
		{
			if(err)
			{
				res.send(JSON.stringify({ "status": "failure","code" : 0,"msg":"Some server side error occured"}));
			}
			else
			{
				if(document[0].notification != undefined)
				{
					var notificationList = document[0].notification;
					var i = 0;
					async.eachSeries(notificationList, function (notification, callback) 
		   			{	
		   				//console.log("----matching id = "+notification.notifyid+", "+notifyid)
						//console.log("----matching id = "+JSON.stringify(notification));

		   				if(notification != undefined && notification.notifyid == notifyid)
		   				{		   					
		   					document[0].notification.splice(i,1);
							mongoConn.storyColl.save(document[0],function(err,result) {});
							res.send(JSON.stringify({ "status": "success","code" : 1,"msg":"successfully status changed	"}));
		   				}
		   				i++;
		   				callback();
		   			});

				}
			}
		});
	}

};

exports.performance = function(req,res)
{	

	var userid = req.session.userid;
	var hostaddress = "http://"+req.headers.host;

	if(req.session.userid)
	{
		mongoConn.storyColl.find({"_id": ObjectId(userid)}).toArray(function(err, document) 
		{
			if(err)
			{
				res.send(JSON.stringify({ "status": "failure","code" : 0,"msg":"Some server side error occured"}));
			}
			else
			{
				var storylists = document[0].storylist;
				var storylistsLength = storylists.length;
				var totalviews = 0;
				var totalearned = 0;
				var stroyarticlesJson = [];
                var profilepictureurl = '';


				var notificationcount = 0;
                if(document[0].notification != undefined)
                {
                    var notificationList = document[0].notification;
                    var i=0;
                    async.eachSeries(notificationList, function (notification, callback) 
                    {
                        if(notification.notificationstatus == '')i++;
                        callback();
                    });
                    notificationcount = i;
                }
                else
                {
                    notificationcount = 0;
                }
                console.log("!!!!------Performance  - profile arr = "+JSON.stringify(document[0].profilepicture));

                async.eachSeries(document[0].profilepicture, function (profilepicture, profilepicturecallback)
            	{
            		console.log("!!!!------Performance  - profilepicture = "+JSON.stringify(profilepicture));

        		 	var o_id = new mongo.ObjectID(profilepicture);
				    mongoConn.profilepictureColl.find({"_id":o_id}).toArray(function(err,profileimage)
				    {
				        console.log(profileimage);
				        var profilecontenfulid = '';

				        if(profileimage.length >= 1)
				        {
				            //var path = profileimage[0].path;
				            //profilepictureurl = hostaddress+"/"+path.split("public/")[1];	
				            profilepictureurl = profileimage[0].profilecontenfulimageurl;			       
				        }
				        else
				        {
				            profilepictureurl = hostaddress+"/img/pro-pic.png";
				        }
                		profilepicturecallback();
			        });
            	});

				async.eachSeries(storylists, function (story, callback) 
	   			{	
	   				totalviews = totalviews + parseInt(story.alltimeviews);
					totalearned = totalearned + (totalviews * 0.001);
					var articleViewsEarned =  parseInt(story.alltimeviews) * 0.001;
					//console.log("Total article views earned = "+articleViewsEarned);
					if(story.alltimeviews > 1000)articleViewsEarned = '£'+ (articleViewsEarned.toFixed(2));
					else articleViewsEarned = '£0';

					leftviews = 1000 - parseInt(story.alltimeviews);

					stroyarticlesJson.push({
						"title":story.title,
						"bodycontent":story.bodycontent,
						"views":story.alltimeviews,
						"leftviews":leftviews,
						"earned":articleViewsEarned//.toFixed(3)
					});
					callback();
				})

				if(totalviews > 1000)totalearned = '£'+((totalviews*0.001).toFixed(2));
				else totalearned = '£0';

				var thisweekviewsWritersCount = [];
				var lastweekviewsWritersCount = [];
				var alltimeviewsWritersCount = [];

				var  myarticle_alltimeviews = [];
				var  myarticle_thisweekviews = [];
				var  myarticle_lastweekviews = [];


				mongoConn.storyColl.find().toArray(function(err, documents) 
				{
					async.eachSeries(documents, function (writer, firstcallback) 
	   				{
	   					var alltimeview = 0;
	   					var thisweekviews = 0;
	   					var lastweekviews = 0;
	   					var writername = '';
	   					if(writer.lastname != undefined)writername = writer.username +" "+writer.lastname
	   					else writername = writer.username;	


   						//console.log(JSON.stringify(writer));
   						//console.log(writer._id+" == "+userid);
   						//var imageurl = getWriterImageurl(function(writer.profilepicture[0],function(imgurl){return imgurl}));

	   					async.eachSeries(writer.storylist, function (story, secondcallback) 
		   				{	
		   					alltimeview =  alltimeview + parseInt(story.alltimeviews);
		   					thisweekviews = thisweekviews + parseInt(story.thisweekviews);
		   					lastweekviews = lastweekviews + parseInt(story.lastweekviews);		   					
		   					secondcallback();
		   				});

		   				//console.log("!!!!------Performance  - alltimeview = "+alltimeview);
		   				//console.log("!!!!------Performance  - thisweekviews = "+thisweekviews);
		   				//console.log("!!!!------Performance  - lastweekviews = "+lastweekviews);

		   				if(writer._id == userid)
		   				{
		   					if( alltimeview != undefined)
		   					{
		   						myarticle_alltimeviews.push({"writername":writername,"alltimeviewcount":alltimeview});
		   					}
							if(thisweekviews != undefined){
								myarticle_thisweekviews.push({"writername":writername,"thisweekviewscount":thisweekviews});
							}
							if(lastweekviews != undefined){
								myarticle_lastweekviews.push({"writername":writername,"lastweekviewscount":lastweekviews});
							}
		   				}
		   				else
		   				{
	   						if(alltimeview != undefined)
		   					{
		   						alltimeviewsWritersCount.push({"writername":writername,"alltimeviewcount":alltimeview});
		   					}
							if(thisweekviews != undefined){
								thisweekviewsWritersCount.push({"writername":writername,"thisweekviewscount":thisweekviews});
							}
							if(lastweekviews != undefined){
								lastweekviewsWritersCount.push({"writername":writername,"lastweekviewscount":lastweekviews});
							}
		   				}	   				

		   				firstcallback();
	   				});	

					if(profilepictureurl == '')profilepictureurl = hostaddress+"/img/pro-pic.png";

	   				console.log("Profile imageulr = "+profilepictureurl);

					res.render('performance', { 
						title: 'Express',
						notificationcount:notificationcount,
						storylistsCount:storylistsLength,
						totalviewsCount:totalviews,
						totalearned : totalearned,
						storylists:stroyarticlesJson,
						thisweekviewsWritersCount:thisweekviewsWritersCount,
						lastweekviewsWritersCount:lastweekviewsWritersCount,
						alltimeviewsWritersCount:alltimeviewsWritersCount,
						myarticle_alltimeviews:myarticle_alltimeviews,
						myarticle_thisweekviews:myarticle_thisweekviews,
						myarticle_lastweekviews :myarticle_lastweekviews,
						profilepictureurl:profilepictureurl
					});
				});	
			}
		});
	}
	else
	{
 		res.redirect('/');	
	}
	
};


exports.saveSignupForm = function(req,res)
{
	console.log(JSON.stringify(req.body));
	var email = req.body.email;
	var password = req.body.password;
	var repassword = req.body.repassword;

	//var json = {"title":req.body.title,"subheading":req.body.subheading,"bodycontent":req.body.bodycontent,"storyid":"11","storypicture":[],"createddate":"","modifieddate":"","storystatus":"In progress","sotrycategory":""};

	var userjson = {
		"writerid" : "",
		"username" : "",
		"userid":email,
		"lastname" : "",
		"password" : password,
		"universityname" : "",
		"city" : "",
		"email" : email,
		"facebooklinks" : "",
		"twitterlinks" : "",
		"aboutyou":"",
		"profilepicture" : [ ],
		"storylist" :[],
		"profileupdate":"false"};

	mongoConn.storyColl.find({email: email}).toArray(function(err, document) 
	{
		if(err)
		{
			res.send(JSON.stringify({ "status": "failure","code" : 0,"msg":"Some server side error occured"}));
		}
		else
		{
			console.log(document.length);
			console.log(JSON.stringify(document));

			if(document.length >= 1)
			{
				//req.session.userid = email;
    			res.send(JSON.stringify({ "status": "failure","code" : 0,"msg":"User already exists"}));
			}
			else
			{
				client.getSpace(space_id)
				.then((space) => {
					space.createEntry('writer', {
				     	fields: {
				       		email: {
								'en-US': email
				       		},
				       		password: {
								'en-US': password
				       		},
				       		name: {
								'en-US': ""
				       		}
				     	}
				   	})
				   	.then((user) => {
				   		console.log(user.sys.id);	 
				   		userjson.writerid = user.sys.id
				   		mongoConn.storyColl.insert(userjson,function(err, result) 
						{
							console.log(JSON.stringify(result));
							console.log(result.ops[0].email);	
							console.log("Documentid = "+result.ops[0]._id);					
							req.session.userid = result.ops[0].email;
							req.session.writerid = user.sys.id;
		    				res.send(JSON.stringify({ "status": "success","code" : 1,"msg":"User created successfully"}));						
						});  		
				   	})
			    })				
			}
		}	
	});
};

exports.logoutWriter = function(req,res)
{
	req.session.destroy();
	res.redirect('/');	
};

exports.sigininAction = function(req,res)
{
	console.log(JSON.stringify(req.body));
	var email = req.body.email;
	var password = req.body.password;

	mongoConn.storyColl.find({email: email, password:password}).toArray(function(err, document) 
	{
		if(err)
		{
			res.send(JSON.stringify({ "status": "failure","code" : 0,"msg":"Some server side error occured"}));
		}
		else
		{
			console.log(document.length);
			console.log(JSON.stringify(document));

			if(document.length >= 1)
			{
				console.log("mongocollection userid = "+document[0]._id);
				req.session.userid = document[0]._id;
				req.session.writerid = document[0].writerid;
				res.send(JSON.stringify({ "status": "success","code" : 1,"msg":"User loginIn successfully"}));
			}	
			else
			{				
				res.send(JSON.stringify({ "status": "success","code" :0,"msg":"User credentials not correct"}));
			}
		}	
	});
};

exports.sigininSocialAction = function(req,res)
{
	var username = req.body.username;
	var userid = req.body.userid;
	var password = req.body.userid;
	var authtype = req.body.authtype;
	var email = req.body.email;
	var largepicture = req.body.largepicture;
	var smallpicture = req.body.smallpicture;

	console.log("!!!!!-----sigininSocialAction 1 = "+JSON.stringify(req.body));

	var userjson = {
		"writerid" : "",
		"userid":userid,
		"username" : "",
		lastname:"",
		"password" : password,
		"universityname" : "",
		"city" : "",
		"email" : email,
		"facebooklinks" : "",
		"twitterlinks" : "",
		"aboutyou":"",
		"profilepicture" : [],
		"storylist" :[],
		"profileupdate":"false"};

	mongoConn.storyColl.find({"userid": userid}).toArray(function(err, document) 
	{
		if(err)
		{
			res.send(JSON.stringify({ "status": "failure","code" : 0,"msg":"Some server side error occured"}));
		}
		else
		{
			console.log("!!!!!-----sigininSocialAction 2 = "+document.length);
			//console.log("!!!!!-----sigininSocialAction = "+JSON.stringify(document));

			if(document.length >= 1)
			{
				req.session.userid = document[0]._id;
				req.session.writerid = document[0].writerid;
    			res.send(JSON.stringify({ "status": "success","code" : 1,"msg":"User already exists"}));
			}
			else
			{

				if(largepicture != '' || smallpicture != '')
				{

					if(largepicture == undefined && smallpicture != undefined) largepicture = smallpicture;
					imageUrl = largepicture;
					var profilecontenfulid = '';					
					var profilecontenfulimageurl = '';

					client.getSpace(space_id)
				    .then((space) => {
				    	console.log("!!!!!-----sigininSocialAction 3 = Getting Space successfully");
				    	console.log("!!!!!-----sigininSocialAction 32 = "+imageUrl);
						space.createAsset({
					      fields: {
					      	title: {
					      		'en-US' : imageUrl.replace(/^.*[\\\/]/, '')
					      	},
					        file: {
					          'en-US': {
					             contentType: 'image/jpeg',
					             fileName: imageUrl.replace(/^.*[\\\/]/, ''),
					             upload: imageUrl
					          }
					        }
					      }
					    })
					    .then(function(e) 
					    {			
							e.processForAllLocales().then(function(data) {
								data.publish()
								profilecontenfulid = e.sys.id;
								profilecontenfulimageurl = data.fields.file['en-US'] != undefined ? data.fields.file['en-US'].url : '';

								console.log("!!!!!-----sigininSocialAction 4 = Social picture asset id = "+profilecontenfulid);
								console.log("!!!!!-----sigininSocialAction 5 = Social picture asset url =  "+profilecontenfulimageurl);

								console.log("!!!!!-----sigininSocialAction 6 = Social picture asset created ");
								var requestfile = {
									"fieldname" : "profilepicturefile",
									"originalname" : largepicture,
									"encoding" : "7bit",
									"mimetype" : "image/jpeg",
									"destination" : largepicture,
									"filename" : largepicture,
									"path" : largepicture,
									"size" : 13582,
									"profilecontenfulid":profilecontenfulid,
									"profilecontenfulimageurl":profilecontenfulimageurl
								}

		                    	mongoConn.profilepictureColl.save(requestfile,function(err,profileresult) 
		                    	{
		                    		console.log("!!!!!-----sigininSocialAction - profilepicturefile collection save "+JSON.stringify(profileresult));
		                		 	var profileresultid = profileresult.ops[0]._id;
		                		 	console.log("!!!!!-----sigininSocialAction - profilepictureid =  "+profileresultid);
		                		 	userjson.profilepicture[0] = profileresultid;

		                    		client.getSpace(space_id)
										.then((space) => {
										console.log("!!!!!-----sigininSocialAction 7 = Get spacing")
										space.createEntry('writer', {
									     	fields: {				       		
									       		password: {
													'en-US': password
									       		},
									       		name:{
									       			'en-US':username
									       		},
									       		writerImage: {	
									       			'en-US' : {
							       						"sys": {
															"type": "Media",
															"linkType": "Asset",
															"id": profilecontenfulid
												        }
												    }
									       		}			
									     	}
									   	})
									   	.then((user) => {
									   		console.log("!!!!!-----sigininSocialAction - writr entry creation = "+user.sys.id);	 
									   		userjson.writerid = user.sys.id
									   		mongoConn.storyColl.save(userjson,function(err, result) 
											{
												console.log("!!!!!-----sigininSocialAction 8 = writer result "+JSON.stringify(result));
												console.log("!!!!!-----sigininSocialAction 9 = writer result = "+result.ops[0].email);
												req.session.userid = result.ops[0]._id;
												//req.session.userid = result.ops[0].email;
												req.session.writerid = user.sys.id;
							    				res.send(JSON.stringify({ "status": "success","code" : 1,"msg":"User created successfully"}));
											});  		
									   	})
								    })
		                    	});	
								//return {sysid : data.sys.id, path : data.fields.field['en-US'].url, name: data.fields.field['en-US'].fileName};
							})
						})
					})

				}
				else
				{	
					client.getSpace(space_id)
					.then((space) => {
						console.log("!!!!!-----sigininSocialAction = Get spacing")
						space.createEntry('writer', {
					     	fields: {				       		
					       		password: {
									'en-US': password
					       		},
					       		name:{
					       			'en-US':username
					       		}
					     	}
					   	})
					   	.then((user) => {
					   		console.log("!!!!!-----sigininSocialAction - writr entry creation = "+user.sys.id);	 
					   		userjson.writerid = user.sys.id
					   		mongoConn.storyColl.insert(userjson,function(err, result) 
							{
								console.log("!!!!!-----sigininSocialAction "+JSON.stringify(result));
								console.log("!!!!!-----sigininSocialAction "+result.ops[0].email);
								req.session.userid = result.ops[0]._id;
								//req.session.userid = result.ops[0].email;
								req.session.writerid = user.sys.id;
			    				res.send(JSON.stringify({ "status": "success","code" : 1,"msg":"User created successfully"}));
							});  		
					   	})
				    })
				}				
			}
		}	
	});

	
};

exports.submitArticletoContentful = function(req,res)
{

	if(req.session.userid)
	{
		var userid = req.session.userid;
		var storyid = req.session.storyid;
		var hostaddress = "http://"+req.headers.host;
		var imageUrl = '';
		var subtitle1 = '';
		var subbodycontent1 = '';
		var subimage1 = '';

		var subtitle2 = '';
		var subbodycontent2 = '';
		var subimage2 = '';

		var subtitle3 = '';
		var subbodycontent3 = '';
		var subimage3 = '';

		var subtitle4 = '';
		var subbodycontent4 = '';
		var subimage4 = '';

		var subtitle5 = '';
		var subbodycontent5 = '';
		var subimage5 = '';

		var subtitle6 = '';
		var subbodycontent6 = '';
		var subimage6 = '';

		var subtitle7 = '';
		var subbodycontent7 = '';
		var subimage7 = '';

		var subtitle8 = '';
		var subbodycontent8 = '';
		var subimage8 = '';

		var subtitle9 = '';
		var subbodycontent9 = '';
		var subimage9 = '';

		var subtitle10 = '';
		var subbodycontent10 = '';
		var subimage10 = '';

		mongoConn.storyColl.find({"_id": ObjectId(userid)}).toArray(function(err, document) 
		{
			if(err)
			{
				res.send(JSON.stringify({ "status": "failure","code" : 0,"msg":"Some server side error occured"}));
			}
			else
			{
				console.log(document.length);
				//console.log(JSON.stringify(document));
				if(document.length >= 1)
				{
					var storylist = document[0].storylist;
					req.session.writerid = document[0].writerid;

					console.log("------------------------------------Writerid = "+writerid);
					console.log(JSON.stringify(document[0].storylist));
					console.log("------------------------------------------"+req.session.writerid);
					var storyval = _.filter(document[0].storylist, function(q){return q.storyid == storyid;});
					var story = storyval[0];
					//console.log("story = "+JSON.stringify(story));
					var subheading = story.subheading;
					var title = story.title;
					var bodycontent = story.bodycontent;
					var storypicture = 0;		
					var articleid;

					if(req.session.category == "Listicle")
					{
						if (story.subtitle1 != '') subtitle1 = story.subtitle1;
						if (story.subbodycontent1 != '') subbodycontent1 = story.subbodycontent1;
						if (story.subimage1 != '') subimage1 = story.subimage1;

						if (story.subtitle2 != '') subtitle2 = story.subtitle2;
						if (story.subbodycontent2 != '') subbodycontent2 = story.subbodycontent2;
						if (story.subimage2 != '') subimage2 = story.subimage2;

						if (story.subtitle3 != '') subtitle3 = story.subtitle3;
						if (story.subbodycontent3 != '') subbodycontent3 = story.subbodycontent3;
						if (story.subimage3 != '') subimage3 = story.subimage3;

						if (story.subtitle4 != '') subtitle4 = story.subtitle4;
						if (story.subbodycontent4 != '') subbodycontent4 = story.subbodycontent4;
						if (story.subimage4 != '') subimage4 = story.subimage4;

						if (story.subtitle5 != '') subtitle5 = story.subtitle5;
						if (story.subbodycontent5 != '') subbodycontent5 = story.subbodycontent5;
						if (story.subimage5 != '') subimage5 = story.subimage5;

						if (story.subtitle6 != '') subtitle6 = story.subtitle6;
						if ( story.subbodycontent6 != '') subbodycontent6 = story.subbodycontent6;
						if (story.subimage6 != '') subimage6 = story.subimage6;

						if (story.subtitle7 != '') subtitle7 = story.subtitle7;
						if (story.subbodycontent7 != '') subbodycontent7 = story.subbodycontent7;
						if (story.subimage7 != '') subimage7 = story.subimage7;

						if (story.subtitle8 != '') subtitle8 = story.subtitle8;
						if (story.subbodycontent8 != '') subbodycontent8 = story.subbodycontent8;
						if (story.subimage8 != '') subimage8 = story.subimage8;

						if (story.subtitle9 != '') subtitle9 = story.subtitle9;
						if (story.subbodycontent9 != '') subbodycontent9 = story.subbodycontent9;
						if (story.subimage9 != '') subimage9 = story.subimage9;

						if (story.subtitle10 != '') subtitle10 = story.subtitle10;
						if (story.subbodycontent10 != '') subbodycontent10 = story.subbodycontent10;
						if (story.subimage10 != '') subimage10 = story.subimage10;
					}
					if(req.session.category == "Gallery")
					{
						if (story.subbodycontent1 != '') subbodycontent1 = story.subbodycontent1;
						if (story.subimage1 != '') subimage1 = story.subimage1;

						if (story.subbodycontent2 != '') subbodycontent2 = story.subbodycontent2;
						if (story.subimage2 != '') subimage2 = story.subimage2;

						if (story.subbodycontent3 != '') subbodycontent3 = story.subbodycontent3;
						if (story.subimage3 != '') subimage3 = story.subimage3;

						if (story.subbodycontent4 != '') subbodycontent4 = story.subbodycontent4;
						if (story.subimage4 != '') subimage4 = story.subimage4;

						if (story.subbodycontent5 != '') subbodycontent5 = story.subbodycontent5;
						if (story.subimage5 != '') subimage5 = story.subimage5;

						if ( story.subbodycontent6 != '') subbodycontent6 = story.subbodycontent6;
						if (story.subimage6 != '') subimage6 = story.subimage6;

						if (story.subbodycontent7 != '') subbodycontent7 = story.subbodycontent7;
						if (story.subimage7 != '') subimage7 = story.subimage7;

						if (story.subbodycontent8 != '') subbodycontent8 = story.subbodycontent8;
						if (story.subimage8 != '') subimage8 = story.subimage8;

						if (story.subbodycontent9 != '') subbodycontent9 = story.subbodycontent9;
						if (story.subimage9 != '') subimage9 = story.subimage9;

						if (story.subbodycontent10 != '') subbodycontent10 = story.subbodycontent10;
						if (story.subimage10 != '') subimage10 = story.subimage10;
					}

					if(story.storypicture != undefined && story.storypicture.length > 0)
					{
						storypicture = story.storypicture[0];
					}
					var writerid = req.session.writerid;	

					console.log("storypicture id = "+storypicture);
					console.log("Artile Id = "+story.articleid);
					articleid =story.articleid

					var o_id = new mongo.ObjectID(storypicture);
					mongoConn.profilepictureColl.find({"_id":o_id}).toArray(function(err,profileimage) 
					{
						console.log(profileimage);
						if(profileimage.length >= 1)
						{
							var path = profileimage[0].path;
							console.log("-----------Submit Article content singleprofileimage - "+path);
							imageUrl = hostaddress+"/"+path.split("public/")[1];
						}
						else
						{
							imageUrl = hostaddress+"/img/pro-pic.png";
						}

						//imageUrl = "http://2adpro.com/images/newlogo.png";
						//imageUrl = hostaddress+"/"+
						//console.log("imageUrl = "+imageUrl);
						//console.log("Edit story session = "+req.session.editstory);
						if(req.session.editstory && articleid != undefined)
						{
							var sysid = story.articleid;

							console.log("Article updation articleid = "+sysid);

							console.log("Article updation title = "+title);
							console.log("Article updation subheading = "+subheading);
							console.log("Article updation bodycontent = "+bodycontent);
							console.log("Article updation writerid = "+writerid);

							if(req.session.category == "Article")
							{

								client.getSpace(space_id)
								.then((space) => {

									console.log("Getting Space successfully");

									space.getEntry(sysid)
									.then((oldentry) => {

										//console.log("Article updation oldentryEntry = "+JSON.stringify(oldentry));
										//console.log("Articel sys id = "+oldentry.sys.version);
										//console.log("Article title = "+oldentry.fields.title["en-US"]);
										//console.log("Article articleImageid = "+oldentry.fields.articleImage["en-US"].sys.id);
										//console.log("Article writer id = "+writerid);
										//console.log("Article error = "+err.message);

										var imageid = oldentry.fields.articleImage["en-US"].sys.id;
										console.log("Article Imageid = "+imageid);

										space.createEntryWithId('article', oldentry.sys.version, sysid, {
									     	fields: {
									       		title: {
													'en-US': title
									       		},
									       		subHeading: {
													'en-US': subheading
									       		},
									       		contentBody: {
									       			'en-US' : bodycontent
									       		},
								       			writer: {
									       			'en-US' : {
							       						"sys": {
															"type": "Link",
															"linkType": "Entry",
															"id": writerid
												        }
												    }
									       		},
								       			articleImage: {	
									       			'en-US' : {
							       						"sys": {
															"type": "Media",
															"linkType": "Asset",
															"id": imageid
												        }
												    }
									       		},	
									       		status:{
									       			'en-US' : 'Submited'
									       		},
									       		boost:{
									       			'en-US' : 0
									       		}	       					       		
									     	}
									   	})
									   	.then((user) => {
								   			console.log('content submitted');	
								   			console.log("outputresult = "+JSON.stringify(user));	
								   			var i= 0;
								   			async.eachSeries(storylist, function (prime, callback) 
								   			{									
												if(prime.storyid == storyid)
												{
													document[0].storylist[i].articleid = user.sys.id;
													document[0].storylist[i].storystatus = "Submited";

													mongoConn.storyColl.save(document[0],function(err,result) {
														console.log("final updation completed");
														res.send(JSON.stringify({ "status": "success","code" : 1,"msg":"User created successfully"}));
													});	
												}
												i++;
												callback();
											})																					   		
									   	})							   	
									})
								});
							}
							else if(req.session.category == "Listicle")
							{
								client.getSpace(space_id)
								.then((space) => {

									console.log("Getting Space successfully");

									space.getEntry(sysid)
									.then((oldentry) => {

										//console.log("Article updation oldentryEntry = "+JSON.stringify(oldentry));
										console.log("Articel Lsiticle sys id = "+oldentry.sys.version);
										console.log("Article Lsiticle title = "+oldentry.fields.title["en-US"]);
										console.log("Article Lsiticle articleImageid = "+oldentry.fields.articleImage["en-US"].sys.id);
										console.log("Article Lsiticle writer id = "+writerid);
										console.log("Article Lsiticle error = "+err.message);

										var imageid = oldentry.fields.articleImage["en-US"].sys.id;
										console.log("Article Imageid = "+imageid);

										space.createEntryWithId('listicle', oldentry.sys.version, sysid, {
											fields: {
												title: {
												'en-US': title
												},
												subHeading: {
												'en-US': subheading
												},
												contentBody: {
												'en-US' : bodycontent
												},
												writer: {
												'en-US' : {
												  "sys": {
												    "type": "Link",
												    "linkType": "Entry",
												    "id": writerid
												  }
												}
												},
												listicleImage: { 
													'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": imageid
													  }
													}
												},

												item1title: { 'en-US': subtitle1 },
												item1image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage1
													  }
													}
												},
												item1contentBody: { 'en-US' : subbodycontent1 },

												item2title: { 'en-US': subtitle2 },
												item2image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage2
													  }
													}
												},
												item2contentBody: { 'en-US' : subbodycontent2 },


												item3title: { 'en-US': subtitle3 },
												item3image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage3
													  }
													}
												},
												item3contentBody: { 'en-US' : subbodycontent3 },


												item4title: { 'en-US': subtitle4 },
												item4image: {
												  'en-US' : {
												  "sys": {
												    "type": "Media",
												    "linkType": "Asset",
												    "id": subimage4
												  }
												}
												},
												item4contentBody: { 'en-US' : subbodycontent4 },

												item5title: { 'en-US': subtitle5 },
												item5image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage5
													  }
													}
												},
												item5contentBody: { 'en-US' : subbodycontent5 },

												item6title: { 'en-US': subtitle6 },
												item6image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage6
													  }
													}
												},
												item7contentBody: { 'en-US' : subbodycontent6 },


												item7title: { 'en-US': subtitle7 },
												item7image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage7
													  }
													}
												},
												item7contentBody: { 'en-US' : subbodycontent7 },


												item8title: { 'en-US': subtitle8 },
												item8image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage8
													  }
													}
												},
												item8contentBody: { 'en-US' : subbodycontent8 },


												item9title: { 'en-US': subtitle9 },
												item9image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage9
													  }
													}
												},
												item9contentBody: { 'en-US' : subbodycontent9 },


												item10title: { 'en-US': subtitle10 },
												item10image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage10
													  }
													}
												},
												item10contentBody: { 'en-US' : subbodycontent10 },
												boost:{
									       			'en-US' : 0
									       		}												
											}
  	   									})
									   	.then((user) => {
								   			console.log('content submitted');	
								   			console.log("outputresult = "+JSON.stringify(user));	
								   			var i= 0;
								   			async.eachSeries(storylist, function (prime, callback) 
								   			{									
												if(prime.storyid == storyid)
												{
													document[0].storylist[i].articleid = user.sys.id;
													document[0].storylist[i].storystatus = "Submited";

													mongoConn.storyColl.save(document[0],function(err,result) {
														console.log("final updation completed");
														res.send(JSON.stringify({ "status": "success","code" : 1,"msg":"User created successfully"}));
													});	
												}
												i++;
												callback();
											})																					   		
									   	})	
								   	})
								})
  
							}
							else if(req.session.category == "Gallery")
							{
								client.getSpace(space_id)
								.then((space) => {

									console.log("Getting Space successfully");

									space.getEntry(sysid)
									.then((oldentry) => {

										//console.log("Article updation oldentryEntry = "+JSON.stringify(oldentry));
										//console.log("Articel sys id = "+oldentry.sys.version);
										//console.log("Article title = "+oldentry.fields.title["en-US"]);
										//console.log("Article articleImageid = "+oldentry.fields.articleImage["en-US"].sys.id);
										//console.log("Article writer id = "+writerid);
										//console.log("Article error = "+err.message);

										var imageid = oldentry.fields.articleImage["en-US"].sys.id;
										console.log("Article Imageid = "+imageid);

										space.createEntryWithId('gallery', oldentry.sys.version, sysid, {
											fields: {
												title: {
												'en-US': title
												},
												subHeading: {
												'en-US': subheading
												},
												contentBody: {
												'en-US' : bodycontent
												},
												writer: {
												'en-US' : {
												  "sys": {
												    "type": "Link",
												    "linkType": "Entry",
												    "id": writerid
												  }
												}
												},
												galleryImage: { 
													'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": imageid
													  }
													}
												},

												item1image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage1
													  }
													}
												},
												item1contentBody: { 'en-US' : subbodycontent1 },

												item2image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage2
													  }
													}
												},
												item2contentBody: { 'en-US' : subbodycontent2 },


												item3image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage3
													  }
													}
												},
												item3contentBody: { 'en-US' : subbodycontent3 },


												item4image: {
												  'en-US' : {
												  "sys": {
												    "type": "Media",
												    "linkType": "Asset",
												    "id": subimage4
												  }
												}
												},
												item4contentBody: { 'en-US' : subbodycontent4 },

												item5image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage5
													  }
													}
												},
												item5contentBody: { 'en-US' : subbodycontent5 },

												item6image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage6
													  }
													}
												},
												item7contentBody: { 'en-US' : subbodycontent6 },


												item7image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage7
													  }
													}
												},
												item7contentBody: { 'en-US' : subbodycontent7 },

												item8image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage8
													  }
													}
												},
												item8contentBody: { 'en-US' : subbodycontent8 },

												item9image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage9
													  }
													}
												},
												item9contentBody: { 'en-US' : subbodycontent9 },

												item10image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage10
													  }
													}
												},
												item10contentBody: { 'en-US' : subbodycontent10 },
												boost:{
									       			'en-US' : 0
									       		}												
											}
  	   									})
									   	.then((user) => {
								   			console.log('content submitted');	
								   			console.log("outputresult = "+JSON.stringify(user));	
								   			var i= 0;
								   			async.eachSeries(storylist, function (prime, callback) 
								   			{									
												if(prime.storyid == storyid)
												{
													document[0].storylist[i].articleid = user.sys.id;
													document[0].storylist[i].storystatus = "Submited";

													mongoConn.storyColl.save(document[0],function(err,result) {
														console.log("final updation completed");
														res.send(JSON.stringify({ "status": "success","code" : 1,"msg":"User created successfully"}));
													});	
												}
												i++;
												callback();
											})																					   		
									   	})	
								   	})
								})
  
							}
						}
						else
						{
							if(req.session.category == "Article")
							{
								client.getSpace(space_id)
								.then((space) => {

									console.log("Space created = "+JSON.stringify(space));

									space.createAsset({
								      fields: {
								      	title: {
								      		'en-US' : imageUrl.replace(/^.*[\\\/]/, '')
								      	},
								        file: {
								          'en-US': {
								             contentType: 'image/jpeg',
								             fileName: imageUrl.replace(/^.*[\\\/]/, ''),
								             upload: imageUrl
								          }
								        }
								      }
								    })
								    .then(function(e) {			
										e.processForAllLocales().then(function(data) {
											data.publish()
											return {sysid : data.sys.id, path : data.fields.field['en-US'].url, name: data.fields.field['en-US'].fileName};
										})
								    	console.log("Asset id = "+e.sys.id);
								    	imageid = e.sys.id;
										console.log("spaceid = "+space_id);
										console.log("writerid = "+writerid);
										console.log("imageid  = "+imageid)
										console.log("title = "+title);
										console.log("subheading = "+subheading);
										console.log("bodycontent = "+bodycontent);

										client.getSpace(space_id)
										.then((space) => {
											space.createEntry('article', {
										     	fields: {
										       		title: {
														'en-US': title
										       		},
										       		subHeading: {
														'en-US': subheading
										       		},
										       		contentBody: {
										       			'en-US' : bodycontent
										       		},
									       			writer: {
										       			'en-US' : {
								       						"sys": {
																"type": "Link",
																"linkType": "Entry",
																"id": writerid
													        }
													    }
										       		},
									       			articleImage: {	
										       			'en-US' : {
								       						"sys": {
																"type": "Media",
																"linkType": "Asset",
																"id": imageid
													        }
													    }
										       		},
										       		boost:{
									       			'en-US' : 0
									       			}								       		
										     	}
										   	})
										   	.then((user) => {

										   		/*
									   			console.log('content submitted');	
									   			console.log("outputresult = "+JSON.stringify(user));	
								   				document[0].storylist[storyid-1].articleid = user.sys.id;
												document[0].storylist[storyid-1].storystatus = "Submited";

												mongoConn.storyColl.save(document[0],function(err,result) {
													console.log("final updation completed");
													res.send(JSON.stringify({ "status": "success","code" : 1,"msg":"User created successfully"}));
												});		
												*/

												var i= 0;
									   			async.eachSeries(document[0].storylist, function (prime, callback) 
									   			{									
													if(prime.storyid == storyid)
													{
														document[0].storylist[i].articleid = user.sys.id;
														document[0].storylist[i].storystatus = "Submited";

														mongoConn.storyColl.save(document[0],function(err,result) {
															console.log("final updation completed");
															res.send(JSON.stringify({ "status": "success","code" : 1,"msg":"User created successfully"}));
														});	
													}
													i++;
													callback();
												})			   		
										   	})
										})
								    })
								})	
							}	
							else if(req.session.category == "Listicle")
							{

								client.getSpace(space_id)
								.then((space) => {

									console.log("Space created = "+JSON.stringify(space));
									console.log("Listicle Storycreation = "+req.session.category)
									space.createAsset({
								      fields: {
								      	title: {
								      		'en-US' : imageUrl.replace(/^.*[\\\/]/, '')
								      	},
								        file: {
								          'en-US': {
								             contentType: 'image/jpeg',
								             fileName: imageUrl.replace(/^.*[\\\/]/, ''),
								             upload: imageUrl
								          }
								        }
								      }
								    })
								    .then(function(e) {			
										e.processForAllLocales().then(function(data) {
											data.publish()
											return {sysid : data.sys.id, path : data.fields.field['en-US'].url, name: data.fields.field['en-US'].fileName};
										})
								    	console.log("Asset id = "+e.sys.id);
								    	imageid = e.sys.id;
										console.log("spaceid = "+space_id);
										console.log("writerid = "+writerid);
										console.log("imageid  = "+imageid)
										console.log("title = "+title);
										console.log("subheading = "+subheading);
										console.log("bodycontent = "+bodycontent);

										client.getSpace(space_id)
										.then((space) => {
											space.createEntry('listicle', {
								     		fields: {
												title: {
												'en-US': title
												},
												subHeading: {
												'en-US': subheading
												},
												contentBody: {
												'en-US' : bodycontent
												},
												writer: {
												'en-US' : {
												  "sys": {
												    "type": "Link",
												    "linkType": "Entry",
												    "id": writerid
												  }
												}
												},
												listicleImage: { 
													'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": imageid
													  }
													}
												},

												item1title: { 'en-US': subtitle1 },
												item1image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage1
													  }
													}
												},
												item1contentBody: { 'en-US' : subbodycontent1 },

												item2title: { 'en-US': subtitle2 },
												item2image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage2
													  }
													}
												},
												item2contentBody: { 'en-US' : subbodycontent2 },


												item3title: { 'en-US': subtitle3 },
												item3image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage3
													  }
													}
												},
												item3contentBody: { 'en-US' : subbodycontent3 },


												item4title: { 'en-US': subtitle4 },
												item4image: {
												  'en-US' : {
												  "sys": {
												    "type": "Media",
												    "linkType": "Asset",
												    "id": subimage4
												  }
												}
												},
												item4contentBody: { 'en-US' : subbodycontent4 },

												item5title: { 'en-US': subtitle5 },
												item5image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage5
													  }
													}
												},
												item5contentBody: { 'en-US' : subbodycontent5 },

												item6title: { 'en-US': subtitle6 },
												item6image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage6
													  }
													}
												},
												item7contentBody: { 'en-US' : subbodycontent6 },


												item7title: { 'en-US': subtitle7 },
												item7image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage7
													  }
													}
												},
												item7contentBody: { 'en-US' : subbodycontent7 },


												item8title: { 'en-US': subtitle8 },
												item8image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage8
													  }
													}
												},
												item8contentBody: { 'en-US' : subbodycontent8 },


												item9title: { 'en-US': subtitle9 },
												item9image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage9
													  }
													}
												},
												item9contentBody: { 'en-US' : subbodycontent9 },


												item10title: { 'en-US': subtitle10 },
												item10image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage10
													  }
													}
												},
												item10contentBody: { 'en-US' : subbodycontent10 },
												boost:{
									       			'en-US' : 0
									       		}												
											}
										   	})
										   	.then((user) => {
										   		/*
									   			console.log('content submitted');	
									   			console.log("outputresult = "+JSON.stringify(user));	
								   				document[0].storylist[storyid-1].articleid = user.sys.id;
												document[0].storylist[storyid-1].storystatus = "Submited";

												mongoConn.storyColl.save(document[0],function(err,result) {
													console.log("final updation completed");
													res.send(JSON.stringify({ "status": "success","code" : 1,"msg":"User created successfully"}));
												});		
												*/

												var i= 0;
									   			async.eachSeries(document[0].storylist, function (prime, callback) 
									   			{									
													if(prime.storyid == storyid)
													{
														document[0].storylist[i].articleid = user.sys.id;
														document[0].storylist[i].storystatus = "Submited";

														mongoConn.storyColl.save(document[0],function(err,result) {
															console.log("final updation completed");
															res.send(JSON.stringify({ "status": "success","code" : 1,"msg":"User created successfully"}));
														});	
													}
													i++;
													callback();
												})			   		
										   	})
										})
								    })
								})	
							}
							else if(req.session.category == "Gallery")
							{
								client.getSpace(space_id)
								.then((space) => {

									console.log("Space created = "+JSON.stringify(space));
									console.log("Listicle Storycreation = "+req.session.category)
									space.createAsset({
								      fields: {
								      	title: {
								      		'en-US' : imageUrl.replace(/^.*[\\\/]/, '')
								      	},
								        file: {
								          'en-US': {
								             contentType: 'image/jpeg',
								             fileName: imageUrl.replace(/^.*[\\\/]/, ''),
								             upload: imageUrl
								          }
								        }
								      }
								    })
								    .then(function(e) {			
										e.processForAllLocales().then(function(data) {
											data.publish()
											return {sysid : data.sys.id, path : data.fields.field['en-US'].url, name: data.fields.field['en-US'].fileName};
										})
								    	console.log("Asset id = "+e.sys.id);
								    	imageid = e.sys.id;
										console.log("spaceid = "+space_id);
										console.log("writerid = "+writerid);
										console.log("imageid  = "+imageid)
										console.log("title = "+title);
										console.log("subheading = "+subheading);
										console.log("bodycontent = "+bodycontent);

										client.getSpace(space_id)
										.then((space) => {
											space.createEntry('gallery', {
								     		fields: {
												title: {
												'en-US': title
												},
												subHeading: {
												'en-US': subheading
												},
												contentBody: {
												'en-US' : bodycontent
												},
												writer: {
												'en-US' : {
												  "sys": {
												    "type": "Link",
												    "linkType": "Entry",
												    "id": writerid
												  }
												}
												},
												galleryImage: { 
													'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": imageid
													  }
													}
												},

												item1image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage1
													  }
													}
												},
												item1contentBody: { 'en-US' : subbodycontent1 },

												item2image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage2
													  }
													}
												},
												item2contentBody: { 'en-US' : subbodycontent2 },


												item3image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage3
													  }
													}
												},
												item3contentBody: { 'en-US' : subbodycontent3 },


												item4image: {
												  'en-US' : {
												  "sys": {
												    "type": "Media",
												    "linkType": "Asset",
												    "id": subimage4
												  }
												}
												},
												item4contentBody: { 'en-US' : subbodycontent4 },

												item5image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage5
													  }
													}
												},
												item5contentBody: { 'en-US' : subbodycontent5 },

												item6image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage6
													  }
													}
												},
												item7contentBody: { 'en-US' : subbodycontent6 },

												item7image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage7
													  }
													}
												},
												item7contentBody: { 'en-US' : subbodycontent7 },

												item8image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage8
													  }
													}
												},
												item8contentBody: { 'en-US' : subbodycontent8 },

												item9image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage9
													  }
													}
												},
												item9contentBody: { 'en-US' : subbodycontent9 },

												item10image: {
												  'en-US' : {
													  "sys": {
													    "type": "Media",
													    "linkType": "Asset",
													    "id": subimage10
													  }
													}
												},
												item10contentBody: { 'en-US' : subbodycontent10 },
												boost:{
									       			'en-US' : 0
									       		}												
											}
										   	})
										   	.then((user) => {
										   		/*
									   			console.log('content submitted');	
									   			console.log("outputresult = "+JSON.stringify(user));	
								   				document[0].storylist[storyid-1].articleid = user.sys.id;
												document[0].storylist[storyid-1].storystatus = "Submited";

												mongoConn.storyColl.save(document[0],function(err,result) {
													console.log("final updation completed");
													res.send(JSON.stringify({ "status": "success","code" : 1,"msg":"User created successfully"}));
												});		
												*/

												var i= 0;
									   			async.eachSeries(document[0].storylist, function (prime, callback) 
									   			{									
													if(prime.storyid == storyid)
													{
														document[0].storylist[i].articleid = user.sys.id;
														document[0].storylist[i].storystatus = "Submited";

														mongoConn.storyColl.save(document[0],function(err,result) {
															console.log("final updation completed");
															res.send(JSON.stringify({ "status": "success","code" : 1,"msg":"User created successfully"}));
														});	
													}
													i++;
													callback();
												})			   		
										   	})
										})
								    })
								})	
							}
						}					
					});				
				}
			}
		});
	}
};

//fetch updated profiles

exports.getProfileupdateStatus = function(req,res)
{
	var userid = req.session.userid;
	var processval = req.query.process;
	console.log("Process type = "+processval);

	mongoConn.storyColl.find({"_id": ObjectId(userid)},{ username: 1,universityname:1,city:1}).toArray(function(err, document) 
	{
		if(err)
		{
			res.send(JSON.stringify({ "status": "failure","code" : 0,"msg":"Some server side error occured"}));
		}
		else
		{
			if(processval == "writerstory")
			{
				req.session.storyid = null;
				// this also works
				delete req.session.storyid;

				req.session.editstory = null;
				delete req.session.editstory;
			}
			var profileupdate = '';
			console.log(JSON.stringify(document));
			if(document.length >= 1)
			{
				username = document[0].username;
				lastname =document[0].lastname;
				universityname = document[0].universityname;
				city = document[0].city;

				if(username != '' && lastname != '' && city != '')
				{
					profileupdate = true;
				}
				else
				{
					profileupdate = false;
				}

				console.log("Profileupdate status = "+profileupdate);
			}
			else
			{
				profileupdate = false;
			}
			res.send(JSON.stringify({ "status": "success","code" : 1,"profileupdate":profileupdate}));
		}
	});
};

exports.profileformSubmit = function(req,res)
{
	var userid = req.session.userid;
	console.log(JSON.stringify(req.body));
	var hostaddress = "http://"+req.headers.host;
	var username = req.body.username;
	var lastname = req.body.lastname;
	var universityname = req.body.universityname;
	var city = req.body.city;
	var email = req.body.email;
	var facebooklinks = req.body.facebooklinks;
	var twitterlinks = req.body.twitterlinks;
	var aboutyou = req.body.aboutyou;
	var paypalemail = req.body.paypalemail;



	var interestentertainment = false;
	var interestsports = false;
	var interestnews = false;

	if(req.body.interestentertainment != undefined) interestentertainment  = req.body.interestentertainment;
	if(req.body.interestsports != undefined) interestsports = req.body.interestsports;
	if(req.body.interestnews != undefined) interestnews = req.body.interestnews;

	console.log("!!!!! ----- Profileformsubmit  method calling");
	console.log("!!!!! ----- Profileformsubmit  request parameter = "+JSON.stringify(req.body));

	console.log("!!!!! ----- Profileformsubmit  request parameter = "+ interestentertainment);
	console.log("!!!!! ----- Profileformsubmit  request parameter = "+ interestsports);
	console.log("!!!!! ----- Profileformsubmit  request parameter = "+ interestnews);

	mongoConn.storyColl.find({"_id": ObjectId(userid)}).toArray(function(err, document) 
	{
		if(err)
		{
			res.send(JSON.stringify({ "status": "failure","code" : 0,"msg":"Some server side error occured"}));
		}
		else
		{		
			//var prefix = 'http://';
			//if (facebooklinks != "" && facebooklinks.substr(0, prefix.length) !== prefix) facebooklinks = prefix + facebooklinks;
			//if (twitterlinks != "" && twitterlinks.substr(0, prefix.length) !== prefix) twitterlinks = prefix + twitterlinks;

			console.log("!!!!! ----- Profileformsubmit = "+JSON.stringify(document));

			if(document.length > 0)
			{
				document[0].username = username;
				document[0].lastname = lastname;
				document[0].universityname = universityname;
				document[0].city = city;
				document[0].email = email;
				document[0].facebooklinks = facebooklinks; 
				document[0].twitterlinks = twitterlinks;
				document[0].aboutyou =  aboutyou;
				document[0].paypalemail = paypalemail;	
				document[0].interestentertainment = interestentertainment;
				document[0].interestsports = interestsports;
				document[0].interestnews = interestnews;


				console.log("!!!!! ----- profileformsubmit - method image id checking");

				if (req.body.hasOwnProperty('imageid') && req.body.imageid != '') 
				{
					document[0].profilepicture = [];
					document[0].profilepicture.push(req.body.imageid);
				}

				if(username != "" && lastname != "" && city != "") document[0].profileupdate = true; 
				else document[0].profileupdate = false;

				console.log("!!!!! ----- profileformsubmit - method writer id checking "+writerid);
				console.log(req.body.hasOwnProperty('writerid') +", "+req.body.hasOwnProperty('writerid'));


				if(req.body.hasOwnProperty('writerid') && req.body.writerid != '')
				{			
					var sysid = req.body.writerid;			
					var profilepictureid = 0;
					if(document[0].profilepicture != undefined && document[0].profilepicture.length > 0)
					{
						profilepictureid = document[0].profilepicture[0];
					}

					var profilecontenfulid = '';
					var o_id = new mongo.ObjectID(profilepictureid);

					console.log("!!!!! ----- profileformsubmit - "+profilepictureid+", "+hostaddress);
				
					createAssetid(profilepictureid,hostaddress,function(profilecontenfulid)
					{	
						console.log("!!!!! ----- profileformsubmit - image id = "+profilecontenfulid);
						console.log("!!!!! ----- profileformsubmit - Sysid - writerid = "+sysid);
						client.getSpace(space_id)
						.then((space) => {
							console.log("!!!!! ----- profileformsubmit - Getting Space successfully");
							console.log("!!!!! ----- profileformsubmit - writer profilecontenfulid = "+profilecontenfulid);
							console.log("!!!!! ----- profileformsubmit - writer id = "+writerid);
							console.log("!!!!! ----- profileformsubmit - writer username = "+username);
							console.log("!!!!! ----- profileformsubmit - writer lastname = "+lastname);
							console.log("!!!!! ----- profileformsubmit - writer email = "+email);
							console.log("!!!!! ----- profileformsubmit - writer twitterlinks = "+twitterlinks);
							console.log("!!!!! ----- profileformsubmit - writer facebooklinks = "+facebooklinks);
							console.log("!!!!! ----- profileformsubmit - writer universityname = "+universityname);
							console.log("!!!!! ----- profileformsubmit - writer city = "+city);
							console.log("!!!!! ----- profileformsubmit - writer aboutyou = "+aboutyou);
							console.log("!!!!! ----- profileformsubmit - writer paypalemail = "+paypalemail);

							//console.log(JSON.stringify(space));
							space.getEntry(sysid)
							.then((oldentry) => {

								//console.log("Article updation oldentryEntry = "+JSON.stringify(oldentry));
								//console.log("Articel sys id = "+oldentry.sys.version);
								//console.log("Article title = "+oldentry.fields.title["en-US"]);
								//console.log("Article articleImageid = "+oldentry.fields.articleImage["en-US"].sys.id);						
								//console.log("Article error = "+err.message);
								//var imageid = oldentry.fields.articleImage["en-US"].sys.id;
								//console.log("Article Imageid = "+imageid);
								//oldentry.publish().then(function(entry){
							   	//		console.log('!!!!! ----- profileformsubmit publish- '+entry.sys.publishedVersion);
							   	//	});

								space.createEntryWithId('writer', oldentry.sys.version, sysid, {
							     	fields: {
							     		name:{
											'en-US': username
							     		},
						     			lastName:{
											'en-US': lastname
							     		},
							       		email: {
											'en-US': email
							       		},
							       		twitterUrl: {
											'en-US': twitterlinks
							       		},
							       		facebookUrl: {
							       			'en-US' : facebooklinks
							       		},
							       		university: {
							       			'en-US' : universityname
							       		},
							       		city: {
							       			'en-US' : city
							       		},
							       		about: {
							       			'en-US' : aboutyou
							       		},
							       		paypalEmail: {
											'en-US' : paypalemail
							       		},
					       				writerImage: {	
							       			'en-US' : {
					       						"sys": {
													"type": "Media",
													"linkType": "Asset",
													"id": profilecontenfulid
										        }
										    }
							       		}						       			       					       		
							     	}
							   	})
							   	.then((user) => {
							   		user.publish()
							   		
						   			console.log('!!!!! ----- profileformsubmit - writer content submitted');	
						   			//console.log("writer outputresult = "+JSON.stringify(user));	
						   			console.log("writerid = "+user.sys.id)
					   				if(user.sys.id == writerid)
									{
										console.log(city+" == "+document[0].listname+", "+email+" == "+document[0].mailchipemail);

										if(city != document[0].listname || email != document[0].mailchipemail)
										{
											mc = new mcapi.Mailchimp(MAILCHIMP_API_KEY);

											mc.lists.list({}, function(data) 
											{
												//console.log(JSON.stringify(data.data));
												var lists = data.data

										        async.eachSeries(lists, function (list, callback) 
										        {
										        	console.log("------------------------------------------------");
										        	//console.log(JSON.stringify(list));
										        	var listname = list.name;
										        	var listid = list.id;
										        	console.log("------------------------------------------------");
										        	console.log("-----!!!!! = "+listname+", "+listid);
										        	if(listname == city || city	 == "National")
										        	{
														mc.lists.subscribe({ id: listid, email:{email:email}, double_optin :"false" }, function(data) 
													  	{
													  		console.log(JSON.stringify(data));
													  		var euid = data.euid;
													  		var leid = data.leid;
													  		var listname = listname;
													  		var listid = listid;	
													  		document[0].euid = euid; 
															document[0].leid = leid;
															document[0].listname =  listname;
															document[0].listid = listid;	
															document[0].mailchipemail = email;

													  		mongoConn.storyColl.save(document[0],function(err,result) 
															{									
																res.send(JSON.stringify({ "status": "success","code" : 1,"msg":"profile updated successfully"}));										
								     						});											  		
														},
													    function(error) 
													    {
													      if (error.error) 
													      {
													       	console.log("!!!!! ----- profileformsubmit - Error occured = "+error.error);
	   										        		callback();

													      } 
													      else 
													      {
													        console.log("!!!!! ----- profileformsubmit - There was an error subscribing that user");
	    									        		callback();

													      }      
													    });
										        	}
										        	else
										        	{
										        		callback();
										        	}
										        });		


									        	mongoConn.storyColl.save(document[0],function(err,result) 
												{									
													res.send(JSON.stringify({ "status": "success","code" : 1,"msg":"profile updated successfully"}));										
					     						});	
											        
											});
										}
										else
										{
											mongoConn.storyColl.save(document[0],function(err,result) 
											{									
												res.send(JSON.stringify({ "status": "success","code" : 1,"msg":"profile updated successfully"}));										
				     						});	
										}								
									}																										   		
							   	})							   	
							}),function(reason) {
								console.log("!!!!! ----- profileformsubmit - Get Entry ERRROR = "+JSON.stringify(reason));
							}
						});							
					});
				}
			}
			else
			{
				res.send(JSON.stringify({ "status": "success","code" : 1,"msg":"profile updated successfully"}));	
			}	
		}
	});
};

exports.updateCategory = function(req,res)
{
	if(req.session.userid)
	{
		console.log("!!!!! Update category by select categorytype page = "+req.body.category);
		req.session.category = req.body.category;
		res.send(JSON.stringify({ "status": "success","code" : 1,"msg":"storytype updated","storytype":req.session.category}));
	}
	
};


exports.uploadimages = function(req,res)
{
	console.log("----------------------------------------------------------------");
	console.log(req.url);
	console.log("----------------------------------------------------------------");
	res.send("."+req.url);
};

exports.getSearchStories = function(req,res)
{
	console.log("searchtext = "+req.body.searchtext);
	mongoConn.storyColl.find({$text: { $search: "25mCMrbXA0SsyuqIcegMsE" } }).sort({writerid:'25mCMrbXA0SsyuqIcegMsE'}).toArray(function(err, document) 
	{
		console.log("searh outputresult = "+JSON.stringify(document));
		res.send(JSON.stringify({ "status": "success","code" : 1,"msg":"storytype updated"}));
	});
};

var task = cron.schedule('* * * * *', function() {
  console.log('Cron started ');

  	client.getSpace(space_id)
	.then((space) => {
		// Now that we have a space, we can get entries from that space 
		space.getEntries({ skip: 1000, limit: 1000, order: 'sys.createdAt' })
		.then((entries) => {
			console.log(entries.items.length);
			//console.log(JSON.stringify(entries));
			
			var i=0;
			async.eachSeries(entries.items, function (prime, callback) {
				i++;
				//console.log("-----------------"+prime.sys.contentType.sys.id);
				//if(prime.fields.title != undefined)console.log("-------Prime result = "+i+") "+prime.fields.title["en-US"]);
				//console.log("------------------------------------------"+prime.sys.contentType.sys.id);
				/*if(prime.fields.writer != undefined && (prime.sys.contentType.sys.id == "writer" || prime.sys.contentType.sys.id == "article" || prime.sys.contentType.sys.id == "gallery") )
				{
					//console.log("-----------------"+prime.sys.contentType.sys.id);
					//console.log("--------------------------------------------------------------1");
					var title = '';
					var subheading = '';
					var bodycontent = '';
					var storystatus = '';
					var note = '';
					//console.log("--------------------------------------------------------------2");
					if(prime.fields.title != undefined)title = prime.fields.title["en-US"]
					//if(prime.fields.subHeading != undefined)subheading = prime.fields.subHeading["en-US"];
					//if(prime.fields.content != undefined)bodycontent = prime.fields.content["en-US"];
					//if(prime.fields.status != undefined)
					//{
						//console.log("-------------------story status = "+JSON.stringify(prime.fields));
						//storystatus = prime.fields.status["en-US"][0];						
					//}
					//if(prime.fields.note != undefined)note = prime.fields.note["en-US"];
					//console.log("Writer id = "+prime.fields.writer["en-US"].sys.id);
					//if(prime.fields.writer["en-US"] != undefined) console.log(prime.fields.writer["en-US"].sys.id);
					var writerid = prime.fields.writer["en-US"].sys.id;
					//console.log("--------------------------------------------------------------3");
					mongoConn.storyColl.find({ writerid:writerid}).toArray(function(err, document) 
					{
						//console.log("Document value = "+JSON.stringify(document));
						//if(prime.fields.title != undefined)console.log(prime.fields.title["en-US"]);
						//console.log("--------------------------------------------------------------4");

						if(document[0] != undefined && document[0].length > 0)
						{
							//console.log("--------------------------------------------------------------5");

							//console.log("--------------start-----------------"+i);
							//console.log(JSON.stringify(prime));
							//console.log("Writer id = "+prime.fields.writer["en-US"].sys.id);
							//console.log("Articleid = "+prime.sys.id);
							var articleid = prime.sys.id;
							//console.log("Writerid =  "+prime.fields.writer.sys.id);	
							//console.log("writerid =  "+prime.fields.writer["en-US"].sys.id);
							//console.log("=================="+JSON.stringify(document[0]));
							//console.log(JSON.stringify(document[0].storylist));
		  					//console.log("---------------end----------------");
							async.eachSeries(document[0].storylist, function (story, innercallback) 
							{
								//console.log("--------------------------------------------------------------6");

								if(story.articleid == articleid)
								{
									//story.title = title;
									//story.subheading = subheading
									//story.bodycontent = bodycontent;
									//console.log("-----------matching Article"+JSON.stringify(story)+", "+storystatus);
									if(storystatus != '')
									{
										console.log("--------------------------------------------------------------7 = "+JSON.stringify(prime));

										story.storystatus = storystatus;
										story.note = note;
									}
							
								}
								innercallback();
							});
							mongoConn.storyColl.save(document[0],function(err,result) {
								//console.log("--------------------------------------------------------------8");

						  		callback(); // Alternatively: callback(new Error());
							});
						}
						else
						{
					  		callback(); // Alternatively: callback(new Error());	
						}					

					});
				}*/
				if(prime.sys.contentType.sys.id == "notification")
				{
					console.log("-----------------Notitficationid = "+prime.sys.contentType.sys.id);
					//console.log(JSON.stringify(prime));
					//var writerid = prime.fields.city["en-US"].sys.id;
					console.log("-----------------Notification started");
					console.log(JSON.stringify(prime));
					
					console.log("-----------------Notification = "+JSON.stringify(prime.fields.city['en-US']));

					var notifyid = '';
					var createdAt = '';
					var subject = '';
					var messagebody = '';
					var city = '';

					if(prime.sys.id != undefined) notifyid = prime.sys.id;
					if(prime.sys.createdAt != undefined) createdAt = prime.sys.createdAt;
					if(prime.fields.subject['en-US']!= undefined) subject = prime.fields.subject['en-US'];
					if(prime.fields.messagebody['en-US'] != undefined) messagebody = prime.fields.messagebody['en-US'];
					if(prime.fields.city['en-US'] != undefined) city = prime.fields.city['en-US'];

					console.log("-----------------Notification notifyid = "+notifyid);
					console.log("-----------------Notification createdat = "+createdAt);
					console.log("-----------------Notification subject = "+subject);
					console.log("-----------------Notification messagebody = "+messagebody);
					console.log("-----------------Notification city = "+city);



					mongoConn.storyColl.find({ city:city}).toArray(function(err, document) 
					{
						if(document[0] != undefined || document.length > 0)
						{
							async.eachSeries(document, function (writerData, innercallback) 
							{
								console.log("Comming city = "+city+", "+prime.fields.city['en-US']);

								if(writerData.city == city || city == "National")
								{
									var notificatData = { 
										notifyid:notifyid,
										subject:subject,
										messagebody:messagebody,
										city:city,
										notificationstatus:"",
										createdAt:createdAt
									};	

									//console.log("-----------------Notification before notification checking = "+writerData.notification);
									if(writerData.notification == undefined)writerData.notification = [];
									//console.log("-----------------Notification After notification checking = "+writerData.notification +", "+writerData.notification.length);
									if(writerData.notification.length == 0)writerData.notification.push(notificatData);
									else
									{
										var i = 0;
										async.eachSeries(writerData.notification, function (writernotificationData, innerlevelcallback) 
										{
											if(writernotificationData.notifyid == notifyid)
											{
												writernotificationData.subject = subject;
												writernotificationData.messagebody = messagebody;
												writernotificationData.city = city;
												writernotificationData.notificationstatus = "";
												writernotificationData.createdAt = createdAt;
												i++;
											}											
											innerlevelcallback();
										});		

										if(i == 0)writerData.notification.push(notificatData);
									}			
												
									mongoConn.storyColl.save(writerData,function(err,result) {});
								}
								innercallback();
							});

						  	callback(); // Alternatively: callback(new Error());
						}
						else
						{
							if(city == "National")
							{
								mongoConn.storyColl.find().toArray(function(err, document) 
								{
									if(document[0] != undefined || document.length > 0)
									{
										async.eachSeries(document, function (writerData, innercallback) 
										{
											console.log("-----------------Comming city = "+city);
											
												var notificatData = { 
													notifyid:notifyid,
													subject:subject,
													messagebody:messagebody,
													city:city,
													notificationstatus:"",
													createdAt:createdAt
												};	

												//console.log("-----------------Notification before notification checking = "+writerData.notification);
												if(writerData.notification == undefined)writerData.notification = [];
												//console.log("-----------------Notification After notification checking = "+writerData.notification +", "+writerData.notification.length);
												if(writerData.notification.length == 0)writerData.notification.push(notificatData);
												else
												{
													var i = 0;
													async.eachSeries(writerData.notification, function (writernotificationData, innerlevelcallback) 
													{
														if(writernotificationData.notifyid == notifyid)
														{
															writernotificationData.subject = subject;
															writernotificationData.messagebody = messagebody;
															writernotificationData.city = city;
															writernotificationData.notificationstatus = "";
															writernotificationData.createdAt = createdAt;
															i++;
														}											
														innerlevelcallback();
													});		

													if(i == 0)writerData.notification.push(notificatData);
												}			
															
												mongoConn.storyColl.save(writerData,function(err,result) 
												{
													innercallback();
												});
										});

									  	callback(); // Alternatively: callback(new Error());
									}
									else
									{
										console.log("--------------------------------------------------------------else condition");
						  				callback(); // Alternatively: callback(new Error());			
									}					
								});								  		
							}
							else
							{
								callback();
							}						
						}					

					});		
				}
				else
				{

					callback();			
				}


			}, function (err) {
			  if (err) { 
			  	console.log(JSON.stringify(err)); 
			  }
			  console.log("Total entry count = "+i);
			  console.log('Cron cycle completed :-)!');
			});
			

		})
	})
}, false);
//task.start();
