
/*
* GET users listing.
*/
var mongo = require('mongodb');
var mongoConn = require('../routes/connection');
var contentful = require('contentful-management'); 
var config = require('config');

var space_id =  process.env.CONTENTFUL_SPACE_ID;
var accessToken = process.env.CONTENTFUL_ACCESS_TOKEN;
var MAILCHIMP_API_KEY = process.env.MAILCHIMP_API_KEY ;


var async = require('async');
var ObjectId = require('mongodb').ObjectID;
var fs = require('fs');
var moment = require('moment');

var showdown  = require('showdown');
var converter = new showdown.Converter();
   

var client = contentful.createClient({
  // This is the access token for this space. Normally you get both ID and the token in the Contentful web app 
  accessToken: accessToken
})
var shortid = require('shortid');

exports.view = function(req, res)
{
	if(req.session.userid)
	{
		var userid = req.session.userid;
		mongoConn.storyColl.find({"_id": ObjectId(userid)}).toArray(function(err, document) 
		{
			if(!err)
			{		
				var profileupdatestatus = 0;
				console.log(JSON.stringify(document));
				var profileupdatedb = document[0].profileupdate;
				if(profileupdatedb == true) profileupdatestatus = 1;
				else profileupdatestatus = 0;

				storylistlength = document[0].storylist.length;

				console.log("formstory = "+profileupdatestatus);
				var profilepictureid = 0;				

				//console.log(document[0]);
				var profilepictures = document[0].profilepicture;
				console.log(JSON.stringify(profilepictures));

				if(profilepictures != undefined & profilepictures != '')
				{
					profilepictureid = profilepictures[profilepictures.length-1];
				}

				var notificationcount = 0;
                if(document[0].notification != undefined)
                {
                    var notificationList = document[0].notification;
                    var i=0;
                    async.eachSeries(notificationList, function (notification, callback) 
                    {
                        if(notification.notificationstatus == '')i++;
                        callback();
                    });
                    notificationcount = i;
                }
                else
                {
                    notificationcount = 0;
                }

				console.log("Profilepictureid = "+profilepictureid);

				var o_id = new mongo.ObjectID(profilepictureid);
				mongoConn.profilepictureColl.find({"_id":o_id}).toArray(function(err,profileimage) 
				{
					console.log("Profileimagearr = "+JSON.stringify(profileimage));
					if(profileimage.length >= 1)
					{
						//var path = profileimage[0].path;
						//console.log("-----------storr profile view singleprofileimage - "+path);
						//document[0].profilepicture = path.split("public/")[1];
						document[0].profilepicture = profileimage[0].profilecontenfulimageurl;
					}		
					else
					{
						document[0].profilepicture = 'img/pro-pic.png';
					}		
					var username = document[0].username;
					var lastname = document[0].lastname;
					var university = document[0].universityname;
					if(university != "")university = university+", ";
					var city =  document[0].city;
					var facebooklinks = document[0].facebooklinks;
					var twitterlinks = document[0].twitterlinks;
					var profilepictureurl = document[0].profilepicture;
					
	  				res.render('story', { 
	  					title: 'Express',
	  					sessiontrue: 1,profileupdate:0 ,
	  					profileupdatestatus:profileupdatestatus,
	  					storylistlength:storylistlength,
	  					username:username,
	  					lastname:lastname,
	  					university:university,
	  					city:city,
	  					facebooklinks:facebooklinks,
	  					twitterlinks:twitterlinks,
	  					profilepictureurl:profilepictureurl,
	  					notificationcount:notificationcount
	  				});
				});
			}

		});
	}
	else
	{
	 	res.redirect('/');	
	}
};

exports.storyboardview = function(req,res)
{
	console.log("!!!! storyboardview = "+req.params.storytype);
	console.log("!!!! Storyboard category = "+req.session.category);
	if(req.session.userid)
	{
		var storytype = req.params.storytype;
		//req.sesssion.storytype = storytype;

		req.session.category = storytype;
		var sotryid = req.session.storyid;

		if(req.query.hasOwnProperty('storyid') && req.query.storyid != "")
		{
			req.session.storyid = req.query.storyid;
			sotryid = req.session.storyid;
		}

		if(req.query.hasOwnProperty('storystatus') && req.query.storystatus == "edit")
		{
			req.session.editstory = true;
		}

		var userid = req.session.userid;
		var storyWizardTitle = "Write a Story";

		if(req.session.storyid)
		{
			mongoConn.storyColl.find( {"_id":ObjectId(userid)}).toArray(function(err,articleresult)
			{
				//	console.log(articleresult[0]);
				var storyid = req.session.storyid;
				console.log("---storyid = "+storyid)
				//var story = articleresult[0].storylist[storyid-1];
				//console.log("Story lists = "+JSON.stringify(articleresult[0].storylist))
			
				var storyval = _.filter(articleresult[0].storylist, function(q){
					//console.log("findid = storyid : "+q.storyid+" = "+storyid);
					return q.storyid == storyid;
				});
				var story = storyval[0];
				//console.log("----------------------------------------");
				//console.log("find a key using underscore filter = "+JSON.stringify(hasHow));
				//console.log("----------------------------------------");
				var storytype = req.session.category;

				var notificationcount = 0;
                if(articleresult[0].notification != undefined)
                {
                    var notificationList = articleresult[0].notification;
                    var i=0;
                    async.eachSeries(notificationList, function (notification, callback) 
                    {
                        if(notification.notificationstatus == '')i++;
                        callback();
                    });
                    notificationcount = i;
                }
                else
                {
                    notificationcount = 0;
                }

				console.log("-------Story type = "+storytype);

				if(story != undefined && storytype == "Article")
				{
					console.log("story = "+JSON.stringify(story));
					var profileid = 0;
					if(story.storypicture != undefined )
					{
						profileid = story.storypicture[0];
					}

					var o_id = new mongo.ObjectID(profileid);
					mongoConn.profilepictureColl.find({"_id":o_id}).toArray(function(err,profileimage) 
					{
						console.log(profileimage);
						console.log("Review status = "+story.storystatus);
						storyWizardTitle = "Edit a Story";
						if(profileimage.length >= 1)
						{
							var path = profileimage[0].path;
							console.log("-----------singleprofileimage - "+path);
							story.storypicture = '../'+path.split("public/")[1];
							story.storyid = req.session.storyid;						
						}		
						else
						{
							story.storyid = req.session.storyid;
							story.storypicture = '../img/article-pic.png';							
						}	

						res.render('storyboardarticle', {
							title: 'Storyboard',
							storytype:storytype,
							sessiontrue: 1,
							story:story,
							storyWizardTitle:storyWizardTitle,
							notificationcount:notificationcount
						});	
					});
				}
				else if(story != undefined && storytype == "Listicle")
				{

					console.log("------Listicle portion = ");
					console.log("story = "+JSON.stringify(story));
					var profileid = 0;
					if(story.storypicture != undefined )
					{
						profileid = story.storypicture[0];
					}

					var o_id = new mongo.ObjectID(profileid);
					mongoConn.profilepictureColl.find({"_id":o_id}).toArray(function(err,profileimage) 
					{
						console.log(profileimage);
						console.log("Review status = "+story.storystatus);
						storyWizardTitle = "Edit a Story";
						if(profileimage.length >= 1)
						{
							var path = profileimage[0].path;
							console.log("-----------singleprofileimage - "+path);
							story.storypicture = '../'+path.split("public/")[1];
							story.storyid = req.session.storyid;						
						}		
						else
						{
							story.storyid = req.session.storyid;
							story.storypicture = '../img/article-pic.png';							
						}	

						res.render('storyboardlisticle', {
							title: 'Storyboard',
							storytype:storytype,
							sessiontrue: 1,
							story:story,
							storyWizardTitle:storyWizardTitle,
							notificationcount:notificationcount
						});	
					});
				}
				else if(story != undefined && storytype == "Gallery")
				{

					console.log("story = "+JSON.stringify(story));
					var profileid = 0;
					if(story.storypicture != undefined )
					{
						profileid = story.storypicture[0];
					}

					var o_id = new mongo.ObjectID(profileid);
					mongoConn.profilepictureColl.find({"_id":o_id}).toArray(function(err,profileimage) 
					{
						console.log(profileimage);
						console.log("Review status = "+story.storystatus);
						storyWizardTitle = "Edit a Story";
						if(profileimage.length >= 1)
						{
							var path = profileimage[0].path;
							console.log("-----------singleprofileimage - "+path);
							story.storypicture = '../'+path.split("public/")[1];
							story.storyid = req.session.storyid;						
						}		
						else
						{
							story.storyid = req.session.storyid;
							story.storypicture = '../img/article-pic.png';							
						}	
						res.render('storyboardgallery', {
							title: 'Storyboard',
							storytype:storytype,
							sessiontrue: 1,
							story:story,
							storyWizardTitle:storyWizardTitle,
							notificationcount:notificationcount
						});	
					});
				}
				else
				{

					story = [];
					story.storyid = "";
					story.storypicture = '../img/article-pic.png';
					//res.render('storypreview',{title:'StoryPreview',storytype:storytype,sessiontrue: 1,story:story});
					res.render('storyboardarticle',{
						title: 'Storyboard',
						storytype:storytype,
						sessiontrue: 1,
						story:story,
						storyWizardTitle:storyWizardTitle,
						notificationcount:notificationcount
					});
				}
			});
		}
		else
		{
			var story = {title:'',subheading:'',bodycontent:'',storypicture:'../img/article-pic.png',storyid:""};
			mongoConn.storyColl.find( {"_id":ObjectId(userid)}).toArray(function(err,document)
			{

				var notificationcount = 0;
                if(document[0].notification != undefined)
                {
                    var notificationList = document[0].notification;
                    var i=0;
                    async.eachSeries(notificationList, function (notification, callback) 
                    {
                        if(notification.notificationstatus == '')i++;
                        callback();
                    });
                    notificationcount = i;
                }
                else
                {
                    notificationcount = 0;
                }
                
				if(storytype == "Listicle")
				{
					res.render('storyboardlisticle', {
						title: 'Storyboard',
						storytype:"storytype",
						sessiontrue: 1,
						story:story,
						storyWizardTitle:storyWizardTitle,
						notificationcount:notificationcount
					});	
				}
				else if(storytype == "Gallery")
				{
					res.render('storyboardgallery', {
						title: 'Storyboard',
						storytype:storytype,
						sessiontrue: 1,
						story:story,
						storyWizardTitle:storyWizardTitle,
						notificationcount:notificationcount
					});					
				}
				else
				{
					res.render('storyboardarticle',{
						title: 'Storyboard',
						storytype:storytype,
						sessiontrue: 1,
						story:story,
						storyWizardTitle:storyWizardTitle,
						notificationcount:notificationcount
					});

				}	
			});		
		}
	}	
	else
	{
	 	res.redirect('/');	
	}
};

exports.storytypeview = function(req,res)
{
	if(req.session.userid)
	{
		var userid = req.session.userid;
		mongoConn.storyColl.find({"_id": ObjectId(userid)}).toArray(function(err, document) 
		{
			if(!err)
			{	
				console.log("storytypeview");

				var notificationcount = 0;
                if(document[0].notification != undefined)
                {
                    var notificationList = document[0].notification;
                    var i=0;
                    async.eachSeries(notificationList, function (notification, callback) 
                    {
                        if(notification.notificationstatus == '')i++;
                        callback();
                    });
                    notificationcount = i;
                }
                else
                {
                    notificationcount = 0;
                }
			
				res.render('storytype',{title: 'StoryType',sessiontrue: 1,notificationcount:notificationcount});
			}
		});
	}	
	else
	{
	 	res.redirect('/');	
	}
};

exports.storypreview = function(req,res)
{
	var hostaddress = "http://"+req.headers.host;

	if(req.session.userid)
	{
		var userid = req.session.userid;
		var storytype = req.session.category;
		var storyid = req.session.storyid;

		console.log("!!!!! ----- StoryPreview - userid = "+userid);
		console.log("!!!!! ----- StoryPreview - storytype = "+storytype);
		console.log("!!!!! ----- StoryPreview - storyid = "+storyid);

		var profilepictureurl = '';

		if(req.session.storyid)
		{
			mongoConn.storyColl.find( {"_id":ObjectId(userid)}).toArray(function(err,articleresult)
			{
				//console.log(articleresult[0]);
				var storyid = req.session.storyid;
				console.log("!!!!! ----- StoryPreview storyid = "+storyid); 
				//var story = articleresult[0].storylist[storyid-1];

				var profilepicture = '';

    		 	var o_id = new mongo.ObjectID(articleresult[0].profilepicture[0]);
			    mongoConn.profilepictureColl.find({"_id":o_id}).toArray(function(err,profileimage)
			    {
			        console.log("!!!!! ----- StoryPreview - Performnance writer profileimage =" +profileimage);

			        if(profileimage.length >= 1)
			        {
			            profilepictureurl = profileimage[0].profilecontenfulimageurl;
			        }
			        else
			        {
			            profilepictureurl = hostaddress+"/img/pro-pic.png";
			        }
	            	console.log("!!!!! ----- StoryPreview - profilepictureurl = "+profilepictureurl);

					var username = articleresult[0].username+" "+articleresult[0].lastname;
					var storyval = _.filter(articleresult[0].storylist, function(q){return q.storyid == storyid;});
					var story = storyval[0];		

					var notificationcount = 0;
	                if(articleresult[0].notification != undefined)
	                {
	                    var notificationList = articleresult[0].notification;
	                    var i=0;
	                    async.eachSeries(notificationList, function (notification, callback) 
	                    {
	                    	if(notification.notificationstatus == '')i++;
	                        callback();
	                    });
	                    notificationcount = i;
	                }
	                else
	                {
	                    notificationcount = 0;
	                }

	                if(storytype == "Article")
	            	{		
						var storybodycontent_text = story.bodycontent;
						storybodycontent_html  = converter.makeHtml(storybodycontent_text);						
						story.bodycontent = storybodycontent_html;	

						if(story.storypicture != undefined)
						{				
							var profileid = story.storypicture[0];
							var o_id = new mongo.ObjectID(profileid);
							console.log("!!!!! ----- StoryPreview - Article mongoid = "+o_id);
							mongoConn.profilepictureColl.find({"_id":o_id}).toArray(function(err,profileimage) 
							{								
								if(profileimage.length >= 1)
								{
									var path = profileimage[0].path;
									console.log("!!!!! ----- StoryPreview - Article singleprofileimage - "+path);
									story.storypicture = path.split("public/")[1];
									res.render('storypreview',{
										title:'StoryPreview',
										storytype:storytype,
										sessiontrue: 1,
										story:story,
										username:username,
										notificationcount:notificationcount,
										profilepicture:profilepictureurl
									});
								}		
								else
								{
									story.storypicture = '';
									res.render('storypreview',{
										title:'StoryPreview',
										storytype:storytype,
										sessiontrue: 1,
										story:story,
										username:username,
										notificationcount:notificationcount,
										profilepicture:profilepictureurl
									});
								}		
							});
						}
						else
						{
							story.storypicture = '';
							res.render('storypreview',{
								title:'StoryPreview',
								storytype:storytype,
								sessiontrue: 1,
								story:story,
								username:username,
								notificationcount:notificationcount,
								profilepicture:profilepictureurl

							});
						}
					}
					else if(storytype == "Listicle")
					{
						var storypicture = '';
						if(story.storypicture != undefined)
						{
							var profileid = story.storypicture[0];
							var o_id = new mongo.ObjectID(profileid);
							mongoConn.profilepictureColl.find({"_id":o_id}).toArray(function(err,profileimage) 
							{
								console.log(profileimage);
								if(profileimage.length >= 1)
								{
									var path = profileimage[0].path;
									console.log("-----------singleprofileimage - "+path);
									storypicture = path.split("public/")[1];
								}
							});
						}
						else
						{
							storypicture = 'img/article-pic.png';
						}

						var subtitle1 = '';
						var subbodycontent1 = '';
						var subimage1 = '';
						var subimageurl1 = '';

						var subtitle2 = '';
						var subbodycontent2 = '';
						var subimage2 = '';
						var subimageurl2 = '';


						var subtitle3 = '';
						var subbodycontent3 = '';
						var subimage3 = '';
						var subimageurl3 = '';

						var subtitle4 = '';
						var subbodycontent4 = '';
						var subimage4 = '';
						var subimageurl4 = '';

						var subtitle5 = '';
						var subbodycontent5 = '';
						var subimage5 = '';
						var subimageurl5 = '';

						var subtitle6 = '';
						var subbodycontent6 = '';
						var subimage6 = '';
						var subimageurl6 = '';

						var subtitle7 = '';
						var subbodycontent7 = '';
						var subimage7 = '';
						var subimageurl7 = '';

						var subtitle8 = '';
						var subbodycontent8 = '';
						var subimage8 = '';
						var subimageurl8 = '';

						var subtitle9 = '';
						var subbodycontent9 = '';
						var subimage9 = '';
						var subimageurl9 = '';

						var subtitle10 = '';
						var subbodycontent10 = '';
						var subimage10 = '';
						var subimageurl10 = '';

						var listiclecontent = [];
						var storytop = {"title":story.title,"subheading":story.subheading,"bodycontent":story.bodycontent,"storypicture":storypicture};
						
					 	if(story.subtitle1 != '' || story.subbodycontent1 != '' || story.subimage1 !=  '')
					 	{				 		
							listiclecontent.push({"subtitle":story.subtitle1,"subbodycontent":story.subbodycontent1,"subimage":story.subimageurl1});
					 	}
					
						if(story.subtitle2 != '' || story.subbodycontent2 != '' || story.subimage2 !=  '')
					 	{				 		
							listiclecontent.push({"subtitle":story.subtitle2,"subbodycontent":story.subbodycontent2,"subimage":story.subimageurl2});
					 	}

					 	if(story.subtitle3 != '' || story.subbodycontent3 != '' || story.subimage3 !=  '')
					 	{				 		
							listiclecontent.push({"subtitle":story.subtitle3,"subbodycontent":story.subbodycontent3,"subimage":story.subimageurl3});
					 	}

				 		if(story.subtitle4 != '' || story.subbodycontent4 != '' || story.subimage4 !=  '')
					 	{				 		
							listiclecontent.push({"subtitle":story.subtitle4,"subbodycontent":story.subbodycontent4,"subimage":story.subimageurl4});
					 	}

					 	if(story.subtitle5 != '' || story.subbodycontent5 != '' || story.subimage5 !=  '')
					 	{				 		
							listiclecontent.push({"subtitle":story.subtitle5,"subbodycontent":story.subbodycontent5,"subimage":story.subimageurl5});
					 	}
				 		if(story.subtitle6 != '' || story.subbodycontent6 != '' || story.subimage6 !=  '')
					 	{
							listiclecontent.push({"subtitle":story.subtitle6,"subbodycontent":story.subbodycontent6,"subimage":story.subimageurl6});
					 	}

					 	if(story.subtitle7 != '' || story.subbodycontent7 !='' || story.subimage7 !=  '')
					 	{				 		
							listiclecontent.push({"subtitle":story.subtitle7,"subbodycontent":story.subbodycontent7,"subimage":story.subimageurl7});
					 	}

				 		if(story.subtitle8 != '' || story.subbodycontent8 != '' || story.subimage8 !=  '')
					 	{				 		
							listiclecontent.push({"subtitle":story.subtitle8,"subbodycontent":story.subbodycontent8,"subimage":story.subimageurl8});
					 	}

					 	if(story.subtitle9 != '' || story.subbodycontent9 != '' || story.subimage9 !=  '')
					 	{				 		
							listiclecontent.push({"subtitle":story.subtitle9,"subbodycontent":story.subbodycontent9,"subimage":story.subimageurl9});
					 	}

					 	if(story.subtitle10 != '' || story.subbodycontent10 != '' || story.subimage10 !=  '')
					 	{				 		
							listiclecontent.push({"subtitle":story.subtitle10,"subbodycontent":story.subbodycontent10,"subimage":story.subimageurl10});
					 	}				
					 	console.log("----------------");
					 	//console.log("Topcontent = "+JSON.stringify(storytop)) 
					 	//console.log("substoryies = "+JSON.stringify(listiclecontent)) 
						console.log("----------------");
						var storypicture = '';
						if(story.storypicture != undefined)
						{
							var storybodycontent_text = storytop.bodycontent;
							storybodycontent_html  = converter.makeHtml(storybodycontent_text);						
							storytop.bodycontent = storybodycontent_html;	


							var profileid = story.storypicture[0];
							var o_id = new mongo.ObjectID(profileid);
							mongoConn.profilepictureColl.find({"_id":o_id}).toArray(function(err,profileimage) 
							{
								console.log(profileimage);
								if(profileimage.length >= 1)
								{
									var path = profileimage[0].path;
									console.log("-----------singleprofileimage - "+path);
									storytop.storypicture = path.split("public/")[1];
									res.render('storylisticlepreview',{
										title:'StoryPreview',
										storytype:storytype,
										sessiontrue: 1,
										story:storytop,
										substoryes:listiclecontent,
										username:username,
										notificationcount:notificationcount,
										profilepicture:profilepictureurl
									});
								}
								else
								{
									storytop.storypicture = 'img/article-pic.png';
									res.render('storylisticlepreview',{
										title:'StoryPreview',
										storytype:storytype,
										sessiontrue: 1,
										story:storytop,
										substoryes:listiclecontent,
										username:username,
										notificationcount:notificationcount,
										profilepicture:profilepictureurl
									});
								}
							});
						}
					}
					else if(storytype == "Gallery")
					{
						console.log("!!!!! ----- StoryPreview - Gallery checking ...");
						var subbodycontent1 = '';
						var subimage1 = '';
						var subimageurl1 = '';

						var subbodycontent2 = '';
						var subimage2 = '';
						var subimageurl2 = '';

						var subbodycontent3 = '';
						var subimage3 = '';
						var subimageurl3 = '';

						var subbodycontent4 = '';
						var subimage4 = '';
						var subimageurl4 = '';

						var subbodycontent5 = '';
						var subimage5 = '';
						var subimageurl5 = '';

						var subbodycontent6 = '';
						var subimage6 = '';
						var subimageurl6 = '';

						var subbodycontent7 = '';
						var subimage7 = '';
						var subimageurl7 = '';

						var subbodycontent8 = '';
						var subimage8 = '';
						var subimageurl8 = '';

						var subbodycontent9 = '';
						var subimage9 = '';
						var subimageurl9 = '';

						var subbodycontent10 = '';
						var subimage10 = '';
						var subimageurl10 = '';

						var gallerycontent = [];

						var storytop = {"title":story.title,"subheading":story.subheading,"bodycontent":story.bodycontent,"storypicture":storypicture};
						
					 	if(story.subbodycontent1 != '' || story.subimage1 !=  '')
					 	{				 		
							gallerycontent.push({"subbodycontent":story.subbodycontent1,"subimage":story.subimageurl1});
					 	}
					
						if( story.subbodycontent2 != '' || story.subimage2 !=  '')
					 	{				 		
							gallerycontent.push({"subbodycontent":story.subbodycontent2,"subimage":story.subimageurl2});
					 	}

					 	if(story.subbodycontent3 != '' || story.subimage3 !=  '')
					 	{				 		
							gallerycontent.push({"subbodycontent":story.subbodycontent3,"subimage":story.subimageurl3});
					 	}

				 		if( story.subbodycontent4 != '' || story.subimage4 !=  '')
					 	{				 		
							gallerycontent.push({"subbodycontent":story.subbodycontent4,"subimage":story.subimageurl4});
					 	}

					 	if( story.subbodycontent5 != '' || story.subimage5 !=  '')
					 	{				 		
							gallerycontent.push({"subbodycontent":story.subbodycontent5,"subimage":story.subimageurl5});
					 	}
				 		if( story.subbodycontent6 !='' || story.subimage6 !=  '')
					 	{
							gallerycontent.push({"subbodycontent":story.subbodycontent6,"subimage":story.subimageurl6});
					 	}

					 	if( story.subbodycontent7 !='' || story.subimage7 !=  '')
					 	{				 		
							gallerycontent.push({"subbodycontent":story.subbodycontent7,"subimage":story.subimageurl7});
					 	}

				 		if( story.subbodycontent8 !='' || story.subimage8 !=  '')
					 	{				 		
							gallerycontent.push({"subbodycontent":story.subbodycontent8,"subimage":story.subimageurl8});
					 	}

					 	if( story.subbodycontent9 != '' || story.subimage9 !=  '')
					 	{				 		
							gallerycontent.push({"subbodycontent":story.subbodycontent9,"subimage":story.subimageurl9});
					 	}

					 	if( story.subbodycontent10 != '' || story.subimage10 !=  '')
					 	{				 		
							gallerycontent.push({"subbodycontent":story.subbodycontent10,"subimage":story.subimageurl10});
					 	}		

					 	var storybodycontent_text = storytop.bodycontent;
						storybodycontent_html  = converter.makeHtml(storybodycontent_text);						
						storytop.bodycontent = storybodycontent_html;	
						console.log("!!!!! ----- StoryPreview - Gallery storypicture = "+story.storypicture);
					 	var storypicture = '';
						if(story.storypicture != undefined)
						{
							var profileid = story.storypicture[0];
							var o_id = new mongo.ObjectID(profileid);
							mongoConn.profilepictureColl.find({"_id":o_id}).toArray(function(err,profileimage) 
							{
								console.log("!!!!! ----- StoryPreview - Gallery "+profileimage);
								console.log("!!!!! ----- StoryPreview - Gallery profileimage length = "+profileimage.length);

								if(profileimage.length >= 1)
								{
									var path = profileimage[0].path;
									console.log("-----------singleprofileimage - "+path);
									storytop.storypicture = path.split("public/")[1];
									console.log("Gallery story picutre = "+story.storypicture);		
									res.render('storygallerypreview',{
										title:'StoryPreview',
										storytype:storytype,
										sessiontrue: 1,
										story:storytop,
										substoryes:gallerycontent,
										username:username,
										notificationcount:notificationcount,
										profilepicture:profilepictureurl
									});
								}
								else
								{
									storytop.storypicture = 'img/article-pic.png';
									console.log("!!!! ----- StoryPreview - Gallery story picutre = "+story.storypicture);		

									res.render('storygallerypreview',{
										title:'StoryPreview',
										storytype:storytype,
										sessiontrue: 1,
										story:storytop,
										substoryes:gallerycontent,
										username:username,
										notificationcount:notificationcount,
										profilepicture:profilepictureurl
									});
								}
							});
						}
						else
						{
							storytop.storypicture = 'img/article-pic.png';
							console.log("!!!! ----- StoryPreview - Gallery story picutre = "+story.storypicture);		

							res.render('storygallerypreview',{
								title:'StoryPreview',
								storytype:storytype,
								sessiontrue: 1,
								story:storytop,
								substoryes:gallerycontent,
								username:username,
								notificationcount:notificationcount,
								profilepicture:profilepictureurl
							});
						}

					}    	
		        });			
			});
		}
		else
		{
			var story = {'title':'','subheading':'','bodycontent':'','storypicture':''};
			res.render('storypreview',{title:'StoryPreview',storytype:storytype,sessiontrue: 1,story:story,username:username});
		}

	}	
	else
	{
	 	res.redirect('/');	
	}
}

exports.savewriterStory = function(req,res)
{
	console.log("serveraddress = "+req.headers.host);

	if(req.session.userid)
	{
		var userid = req.session.userid;
		mongoConn.storyColl.find({"_id":ObjectId(userid)}).toArray(function(err,writerresult) 
		{

			console.log("Save Story  with storyid = "+req.body.storyid);

			if(req.body.hasOwnProperty('storyid') && req.body.storyid != "")
			{
				var json = {};
				var storylistArry = writerresult[0].storylist;
				var storyid = req.body.storyid;
				var storytype = req.session.category;
				console.log("selected stortype = "+storytype);

				//var story = storylistArry[storyid-1];					
				var storyval = _.filter(writerresult[0].storylist, function(q){return q.storyid == storyid;});
				var story = storyval[0];
				console.log("Story = "+JSON.stringify(story));
				story.title = req.body.title;
				story.subheading =  req.body.subheading;
				story.bodycontent = req.body.bodycontent;
				story.modifieddate = moment().format('ll');
				if (req.body.hasOwnProperty('imageid') && req.body.imageid != '') 
				{
					story.storypicture = [];
					story.storypicture.push(req.body.imageid);				
				}
			
				var i= 0;
				req.session.storyid = storyid;
			   	mongoConn.storyColl.save(writerresult[0],function(err,result) {
			   		res.setHeader('Content-Type', 'application/json');
	    			res.send(JSON.stringify({ "status": "success","storyid":storyid}));
			   	});
			}
			else
			{
				//console.log("writerresult = "+JSON.stringify(writerresult));
				var storylistArry = writerresult[0].storylist;
				//var storylength = storylistArry.length + 1;
				var storylength = shortid.generate();
				var json;
				var storytype = req.session.category;
				console.log("selected stortype = "+storytype);

				var thisweekviews = 0;
				var lastweekviews = 0;
				var alltimeviews =  0;
				var createdate = moment().format('ll');
				var modifieddate = moment().format('ll');
				if (req.body.hasOwnProperty('imageid') && req.body.imageid != '') 
				{
					json = {
						"title":req.body.title,
						"subheading":req.body.subheading,
						"bodycontent":req.body.bodycontent,
						"storypicture":[req.body.imageid],
						"createddate":createdate,
						"modifieddate":modifieddate,
						"storystatus":"In progress",
						"storycategory":storytype,
						"storyid":storylength,
						"thisweekviews":thisweekviews,
						"lastweekviews":lastweekviews,
						"alltimeviews":alltimeviews
					};
				}
				else
				{
					json = {
						"title":req.body.title,
						"subheading":req.body.subheading,
						"bodycontent":req.body.bodycontent,
						"createddate":createdate,
						"modifieddate":modifieddate,
						"storystatus":"In progress",
						"storycategory":storytype,
						"storyid":storylength,
						"thisweekviews":thisweekviews,
						"lastweekviews":lastweekviews,
						"alltimeviews":alltimeviews
					};
				}
				var i= 0;
				req.session.storyid = storylength;
	 			writerresult[0].storylist.push(json);
		     	mongoConn.storyColl.save(writerresult[0],function(err,result) {
		     		res.setHeader('Content-Type', 'application/json');
	    			res.send(JSON.stringify({ "status": "success","storyid":storylength}));
		     	});	     	  	
    		}
		});
	}
	else
	{
	 	res.redirect('/');	
	}
};

exports.saveListicleStory = function(req,res)
{
	//console.log(JSON.stringify(req.body));
	console.log("serveraddress = "+req.headers.host);


	var subtitle1 = '';
	var subbodycontent1 = '';
	var subimage1 = '';
	var subimageurl1 = '';


	var subtitle2 = '';
	var subbodycontent2 = '';
	var subimage2 = '';
	var subimageurl2 = '';

	var subtitle3 = '';
	var subbodycontent3 = '';
	var subimage3 = '';
	var subimageurl3 = '';

	var subtitle4 = '';
	var subbodycontent4 = '';
	var subimage4 = '';
	var subimageurl4 = '';

	var subtitle5 = '';
	var subbodycontent5 = '';
	var subimage5 = '';
	var subimageurl5 = '';

	var subtitle6 = '';
	var subbodycontent6 = '';
	var subimage6 = '';
	var subimageurl6 = '';

	var subtitle7 = '';
	var subbodycontent7 = '';
	var subimage7 = '';
	var subimageurl7 = '';

	var subtitle8 = '';
	var subbodycontent8 = '';
	var subimage8 = '';
	var subimageurl8 = '';

	var subtitle9 = '';
	var subbodycontent9 = '';
	var subimage9 = '';
	var subimageurl9 = '';

	var subtitle10 = '';
	var subbodycontent10 = '';
	var subimage10 = '';
	var subimageurl10 = '';

	if(req.session.userid)
	{
		var userid = req.session.userid;
		mongoConn.storyColl.find({"_id":ObjectId(userid)}).toArray(function(err,writerresult) 
		{

			console.log("Save Story  with storyid = "+req.body.storyid);
			var imageid = [];
			if (req.body.hasOwnProperty('imageid') && req.body.imageid != '') imageid.push(req.body.imageid)

			if (req.body.hasOwnProperty('subtitle1') && req.body.subtitle1 != '') subtitle1 = req.body.subtitle1;
			if (req.body.hasOwnProperty('subbodycontent1') && req.body.subbodycontent1 != '') subbodycontent1 = req.body.subbodycontent1;
			if (req.body.hasOwnProperty('subimage1') && req.body.subimage1 != '') subimage1 = req.body.subimage1;
			if (req.body.hasOwnProperty('subimageurl1') && req.body.subimageurl1 != '') subimageurl1 = req.body.subimageurl1;

			if (req.body.hasOwnProperty('subtitle2') && req.body.subtitle2 != '') subtitle2 = req.body.subtitle2;
			if (req.body.hasOwnProperty('subbodycontent2') && req.body.subbodycontent2 != '') subbodycontent2 = req.body.subbodycontent2;
			if (req.body.hasOwnProperty('subimage2') && req.body.subimage2 != '') subimage2 = req.body.subimage2;
			if (req.body.hasOwnProperty('subimageurl2') && req.body.subimageurl2 != '') subimageurl2 = req.body.subimageurl2;

			if (req.body.hasOwnProperty('subtitle3') && req.body.subtitle3 != '') subtitle3 = req.body.subtitle3;
			if (req.body.hasOwnProperty('subbodycontent3') && req.body.subbodycontent3 != '') subbodycontent3 = req.body.subbodycontent3;
			if (req.body.hasOwnProperty('subimage3') && req.body.subimage3 != '') subimage3 = req.body.subimage3;
			if (req.body.hasOwnProperty('subimageurl3') && req.body.subimageurl3 != '') subimageurl3 = req.body.subimageurl3;

			if (req.body.hasOwnProperty('subtitle4') && req.body.subtitle4 != '') subtitle4 = req.body.subtitle4;
			if (req.body.hasOwnProperty('subbodycontent4') && req.body.subbodycontent4 != '') subbodycontent4 = req.body.subbodycontent4;
			if (req.body.hasOwnProperty('subimage4') && req.body.subimage4 != '') subimage4 = req.body.subimage4;
			if (req.body.hasOwnProperty('subimageurl4') && req.body.subimageurl4 != '') subimageurl4 = req.body.subimageurl4;

			if (req.body.hasOwnProperty('subtitle5') && req.body.subtitle5 != '') subtitle5 = req.body.subtitle5;
			if (req.body.hasOwnProperty('subbodycontent5') && req.body.subbodycontent5 != '') subbodycontent5 = req.body.subbodycontent5;
			if (req.body.hasOwnProperty('subimage5') && req.body.subimage5 != '') subimage5 = req.body.subimage5;
			if (req.body.hasOwnProperty('subimageurl5') && req.body.subimageurl5 != '') subimageurl5 = req.body.subimageurl5;

			if (req.body.hasOwnProperty('subtitle6') && req.body.subtitle6 != '') subtitle6 = req.body.subtitle6;
			if (req.body.hasOwnProperty('subbodycontent6') && req.body.subbodycontent6 != '') subbodycontent6 = req.body.subbodycontent6;
			if (req.body.hasOwnProperty('subimage6') && req.body.subimage6 != '') subimage6 = req.body.subimage6;
			if (req.body.hasOwnProperty('subimageurl6') && req.body.subimageurl6 != '') subimageurl6 = req.body.subimageurl6;

			if (req.body.hasOwnProperty('subtitle7') && req.body.subtitle7 != '') subtitle7 = req.body.subtitle7;
			if (req.body.hasOwnProperty('subbodycontent7') && req.body.subbodycontent7 != '') subbodycontent7 = req.body.subbodycontent7;
			if (req.body.hasOwnProperty('subimage7') && req.body.subimage7 != '') subimage7 = req.body.subimage7;
			if (req.body.hasOwnProperty('subimageurl7') && req.body.subimageurl7 != '') subimageurl7 = req.body.subimageurl7;

			if (req.body.hasOwnProperty('subtitle8') && req.body.subtitle8 != '') subtitle8 = req.body.subtitle8;
			if (req.body.hasOwnProperty('subbodycontent8') && req.body.subbodycontent8 != '') subbodycontent8 = req.body.subbodycontent8;
			if (req.body.hasOwnProperty('subimage8') && req.body.subimage8 != '') subimage8 = req.body.subimage8;
			if (req.body.hasOwnProperty('subimageurl8') && req.body.subimageurl8 != '') subimageurl8 = req.body.subimageurl8;

			if (req.body.hasOwnProperty('subtitle9') && req.body.subtitle9 != '') subtitle9 = req.body.subtitle9;
			if (req.body.hasOwnProperty('subbodycontent9') && req.body.subbodycontent9 != '') subbodycontent9 = req.body.subbodycontent9;
			if (req.body.hasOwnProperty('subimage9') && req.body.subimage9 != '') subimage9 = req.body.subimage9;
			if (req.body.hasOwnProperty('subimageurl9') && req.body.subimageurl9 != '') subimageurl9 = req.body.subimageurl9;

			if (req.body.hasOwnProperty('subtitle10') && req.body.subtitle10 != '') subtitle10 = req.body.subtitle10;
			if (req.body.hasOwnProperty('subbodycontent10') && req.body.subbodycontent10 != '') subbodycontent10 = req.body.subbodycontent10;
			if (req.body.hasOwnProperty('subimage10') && req.body.subimage10 != '') subimage10 = req.body.subimage10;
			if (req.body.hasOwnProperty('subimageurl10') && req.body.subimageurl10 != '') subimageurl10 = req.body.subimageurl10;

			if(req.body.hasOwnProperty('storyid') && req.body.storyid != "")
			{

				var storyid = req.body.storyid;

				var storytype = req.session.category;
				console.log("selected stortype = "+storytype);

				//var story = storylistArry[storyid-1];					
				var storyval = _.filter(writerresult[0].storylist, function(q){return q.storyid == storyid;});
				var story = storyval[0];

				//console.log("Story = "+JSON.stringify(story));


				story.title = req.body.title;
				story.subheading =  req.body.subheading;
				story.bodycontent = req.body.bodycontent;

				story.subtitle1 = subtitle1;
				story.subbodycontent1 = subbodycontent1;
				if(subimage1 != '')story.subimage1 = subimage1;
				if(subimageurl1 != '')story.subimageurl1 = subimageurl1;

				story.subtitle2 = subtitle2;
				story.subbodycontent2 = subbodycontent2;
				if(subimage2 != '')story.subimage2 = subimage2;
				if(subimageurl2 != '')story.subimageurl2 = subimageurl2;

				story.subtitle3 = subtitle3;
				story.subbodycontent3 = subbodycontent3;
				if(subimage3 != '')story.subimage3 = subimage3;
				if(subimageurl3 != '')story.subimageurl3 = subimageurl3;

				story.subtitle4 = subtitle4;
				story.subbodycontent4 = subbodycontent4;
				if(subimage4 != '')story.subimage4 = subimage4;
				if(subimageurl4 != '')story.subimageurl4 = subimageurl4;

				story.subtitle5 = subtitle5;
				story.subbodycontent5 = subbodycontent5;
				if(subimage5 != '')story.subimage5 = subimage5;
				if(subimageurl5 != '')story.subimageurl5 = subimageurl5;

				story.subtitle6 = subtitle6;
				story.subbodycontent6 = subbodycontent6;
				if(subimage6 != '')story.subimage6 = subimage6;
				if(subimageurl6 != '')story.subimageurl6 = subimageurl6;

				story.subtitle7 = subtitle7;
				story.subbodycontent7 = subbodycontent7;
				if(subimage8 != '')story.subimage7 = subimage7;
				if(subimageurl7 != '')story.subimageurl7 = subimageurl7;

				story.subtitle8 = subtitle8;
				story.subbodycontent8 = subbodycontent8;
				if(subimage8 != '')story.subimage8 = subimage8;
				if(subimageurl8 != '')story.subimageurl8 = subimageurl8;

				story.subtitle9 = subtitle9;
				story.subbodycontent9 = subbodycontent9;
				if(subimage9 != '')story.subimage9 = subimage9;
				if(subimageurl9 != '')story.subimageurl9 = subimageurl9;

				story.subtitle10 = subtitle10;
				story.subbodycontent10 = subbodycontent10;
				if(subimage10 != '')story.subimage10 = subimage10;
				if(subimageurl10 != '')story.subimageurl10 = subimageurl10;


				if (req.body.hasOwnProperty('imageid') && req.body.imageid != '') 
				{
					story.storypicture = [];
					story.storypicture.push(req.body.imageid);				
				}
			
				var i= 0;
				req.session.storyid = storyid;
				//storylistArry[storyid-1] = story;
		     	mongoConn.storyColl.save(writerresult[0],function(err,result) {});
	     	  	res.setHeader('Content-Type', 'application/json');
	    		res.send(JSON.stringify({ "status": "success","storyid":storyid}));
			}
			else
			{
				//console.log("writerresult = "+JSON.stringify(writerresult));
				var storylistArry = writerresult[0].storylist;
				//var storylength = storylistArry.length + 1;
				var storylength = shortid.generate();
				var json;
				var storytype = req.session.category;
				console.log("selected stortype = "+storytype);

				var thisweekviews = 0;
				var lastweekviews = 0;
				var alltimeviews =  0;				

				json = {
					"title":req.body.title,
					"subheading":req.body.subheading,
					"bodycontent":req.body.bodycontent,
					"storypicture":imageid,
					"subtitle1":subtitle1,
					"subbodycontent1":subbodycontent1,
					"subimage1":subimage1,
					"subimageurl1":subimageurl1,
					"subtitle2":subtitle2,
					"subbodycontent2":subbodycontent2,
					"subimage2":subimage2,
					"subimageurl2":subimageurl2,
					"subtitle3":subtitle3,
					"subbodycontent3":subbodycontent3,
					"subimage3":subimage3,
					"subimageurl3":subimageurl3,
					"subtitle4":subtitle4,
					"subbodycontent4":subbodycontent4,
					"subimage4":subimage4,
					"subimageurl4":subimageurl4,
					"subtitle5":subtitle5,
					"subbodycontent5":subbodycontent5,
					"subimage5":subimage5,
					"subimageurl5":subimageurl5,
					"subtitle6":subtitle6,
					"subbodycontent6":subbodycontent6,
					"subimage6":subimage6,
					"subimageurl6":subimageurl6,
					"subtitle7":subtitle7,
					"subbodycontent7":subbodycontent7,
					"subimage7":subimage7,
					"subimageurl7":subimageurl7,
					"subtitle8":subtitle8,
					"subbodycontent8":subbodycontent8,
					"subimage8":subimage8,
					"subimageurl8":subimageurl8,
					"subtitle9":subtitle9,
					"subbodycontent9":subbodycontent9,
					"subimage9":subimage9,
					"subimageurl9":subimageurl9,
					"subtitle10":subtitle10,
					"subbodycontent10":subbodycontent10,
					"subimage10":subimage10,
					"subimageurl10":subimageurl10,
					"createddate":"",
					"modifieddate":"",
					"storystatus":"In progress",
					"storycategory":storytype,
					"storyid":storylength,
					"thisweekviews":thisweekviews,
					"lastweekviews":lastweekviews,
					"alltimeviews":alltimeviews
				};
				var i= 0;
				req.session.storyid = storylength;

	 			writerresult[0].storylist.push(json);
		     	mongoConn.storyColl.save(writerresult[0],function(err,result) 
		     	{
	     			res.setHeader('Content-Type', 'application/json');
	    			res.send(JSON.stringify({ "status": "success","storyid":storylength}));
		     	});	     	  
    		}
		});
	}
}

exports.saveGalleryStory = function(req,res)
{
	console.log("serveraddress = "+req.headers.host);

	var subbodycontent1 = '';
	var subimage1 = '';
	var subimageurl1 = '';

	var subbodycontent2 = '';
	var subimage2 = '';
	var subimageurl2 = '';

	var subbodycontent3 = '';
	var subimage3 = '';
	var subimageurl3 = '';

	var subbodycontent4 = '';
	var subimage4 = '';
	var subimageurl4 = '';

	var subbodycontent5 = '';
	var subimage5 = '';
	var subimageurl5 = '';

	var subbodycontent6 = '';
	var subimage6 = '';
	var subimageurl6 = '';

	var subbodycontent7 = '';
	var subimage7 = '';
	var subimageurl7 = '';

	var subbodycontent8 = '';
	var subimage8 = '';
	var subimageurl8 = '';

	var subbodycontent9 = '';
	var subimage9 = '';
	var subimageurl9 = '';

	var subbodycontent10 = '';
	var subimage10 = '';
	var subimageurl10 = '';

	if(req.session.userid)
	{
		var userid = req.session.userid;
		mongoConn.storyColl.find({"_id":ObjectId(userid)}).toArray(function(err,writerresult) 
		{

			console.log("Save Story  with storyid = "+req.body.storyid);
			var imageid = [];
			if (req.body.hasOwnProperty('imageid') && req.body.imageid != '') imageid.push(req.body.imageid)

			if (req.body.hasOwnProperty('subbodycontent1') && req.body.subbodycontent1 != '') subbodycontent1 = req.body.subbodycontent1;
			if (req.body.hasOwnProperty('subimage1') && req.body.subimage1 != '') subimage1 = req.body.subimage1;
			if (req.body.hasOwnProperty('subimageurl1') && req.body.subimageurl1 != '') subimageurl1 = req.body.subimageurl1;

			if (req.body.hasOwnProperty('subbodycontent2') && req.body.subbodycontent2 != '') subbodycontent2 = req.body.subbodycontent2;
			if (req.body.hasOwnProperty('subimage2') && req.body.subimage2 != '') subimage2 = req.body.subimage2;
			if (req.body.hasOwnProperty('subimageurl2') && req.body.subimageurl2 != '') subimageurl2 = req.body.subimageurl2;

			if (req.body.hasOwnProperty('subbodycontent3') && req.body.subbodycontent3 != '') subbodycontent3 = req.body.subbodycontent3;
			if (req.body.hasOwnProperty('subimage3') && req.body.subimage3 != '') subimage3 = req.body.subimage3;
			if (req.body.hasOwnProperty('subimageurl3') && req.body.subimageurl3 != '') subimageurl3 = req.body.subimageurl3;

			if (req.body.hasOwnProperty('subbodycontent4') && req.body.subbodycontent4 != '') subbodycontent4 = req.body.subbodycontent4;
			if (req.body.hasOwnProperty('subimage4') && req.body.subimage4 != '') subimage4 = req.body.subimage4;
			if (req.body.hasOwnProperty('subimageurl4') && req.body.subimageurl4 != '') subimageurl4 = req.body.subimageurl4;

			if (req.body.hasOwnProperty('subbodycontent5') && req.body.subbodycontent5 != '') subbodycontent5 = req.body.subbodycontent5;
			if (req.body.hasOwnProperty('subimage5') && req.body.subimage5 != '') subimage5 = req.body.subimage5;
			if (req.body.hasOwnProperty('subimageurl5') && req.body.subimageurl5 != '') subimageurl5 = req.body.subimageurl5;

			if (req.body.hasOwnProperty('subbodycontent6') && req.body.subbodycontent6 != '') subbodycontent6 = req.body.subbodycontent6;
			if (req.body.hasOwnProperty('subimage6') && req.body.subimage6 != '') subimage6 = req.body.subimage6;
			if (req.body.hasOwnProperty('subimageurl6') && req.body.subimageurl6 != '') subimageurl6 = req.body.subimageurl6;

			if (req.body.hasOwnProperty('subbodycontent7') && req.body.subbodycontent7 != '') subbodycontent7 = req.body.subbodycontent7;
			if (req.body.hasOwnProperty('subimage7') && req.body.subimage7 != '') subimage7 = req.body.subimage7;
			if (req.body.hasOwnProperty('subimageurl7') && req.body.subimageurl7 != '') subimageurl7 = req.body.subimageurl7;

			if (req.body.hasOwnProperty('subbodycontent8') && req.body.subbodycontent8 != '') subbodycontent8 = req.body.subbodycontent8;
			if (req.body.hasOwnProperty('subimage8') && req.body.subimage8 != '') subimage8 = req.body.subimage8;
			if (req.body.hasOwnProperty('subimageurl8') && req.body.subimageurl8 != '') subimageurl8 = req.body.subimageurl8;

			if (req.body.hasOwnProperty('subbodycontent9') && req.body.subbodycontent9 != '') subbodycontent9 = req.body.subbodycontent9;
			if (req.body.hasOwnProperty('subimage9') && req.body.subimage9 != '') subimage9 = req.body.subimage9;
			if (req.body.hasOwnProperty('subimageurl9') && req.body.subimageurl9 != '') subimageurl9 = req.body.subimageurl9;

			if (req.body.hasOwnProperty('subbodycontent10') && req.body.subbodycontent10 != '') subbodycontent10 = req.body.subbodycontent10;
			if (req.body.hasOwnProperty('subimage10') && req.body.subimage10 != '') subimage10 = req.body.subimage10;
			if (req.body.hasOwnProperty('subimageurl10') && req.body.subimageurl10 != '') subimageurl10 = req.body.subimageurl10;

			if(req.body.hasOwnProperty('storyid') && req.body.storyid != "")
			{

				var storyid = req.body.storyid;

				var storytype = req.session.category;
				console.log("selected stortype = "+storytype);

				//var story = storylistArry[storyid-1];					
				var storyval = _.filter(writerresult[0].storylist, function(q){return q.storyid == storyid;});
				var story = storyval[0];

				console.log("Story = "+JSON.stringify(story));

				story.subheading =  req.body.subheading;
				story.bodycontent = req.body.bodycontent;

				story.subbodycontent1 = subbodycontent1;
				if(subimage1 != '')story.subimage1 = subimage1;
				if(subimageurl1 != '')story.subimageurl1 = subimageurl1;

				story.subbodycontent2 = subbodycontent2;
				if(subimage2 != '')story.subimage2 = subimage2;
				if(subimageurl2 != '')story.subimageurl2 = subimageurl2;

				story.subbodycontent3 = subbodycontent3;
				if(subimage3 != '')story.subimage3 = subimage3;
				if(subimageurl3 != '')story.subimageurl3 = subimageurl3;

				story.subbodycontent4 = subbodycontent4;
				if(subimage4 != '')story.subimage4 = subimage4;
				if(subimageurl4 != '')story.subimageurl4 = subimageurl4;

				story.subbodycontent5 = subbodycontent5;
				if(subimage5 != '')story.subimage5 = subimage5;
				if(subimageurl5 != '')story.subimageurl5 = subimageurl5;

				story.subbodycontent6 = subbodycontent6;
				if(subimage6 != '')story.subimage6 = subimage6;
				if(subimageurl6 != '')story.subimageurl6 = subimageurl6;

				story.subbodycontent7 = subbodycontent7;
				if(subimage8 != '')story.subimage7 = subimage7;
				if(subimageurl7 != '')story.subimageurl7 = subimageurl7;

				story.subbodycontent8 = subbodycontent8;
				if(subimage8 != '')story.subimage8 = subimage8;
				if(subimageurl8 != '')story.subimageurl8 = subimageurl8;

				story.subbodycontent9 = subbodycontent9;
				if(subimage9 != '')story.subimage9 = subimage9;
				if(subimageurl9 != '')story.subimageurl9 = subimageurl9;

				story.subbodycontent10 = subbodycontent10;
				if(subimage10 != '')story.subimage10 = subimage10;
				if(subimageurl10 != '')story.subimageurl10 = subimageurl10;


				if (req.body.hasOwnProperty('imageid') && req.body.imageid != '') 
				{
					story.storypicture = [];
					story.storypicture.push(req.body.imageid);				
				}
			
				var i= 0;
				req.session.storyid = storyid;
				//storylistArry[storyid-1] = story;
		     	mongoConn.storyColl.save(writerresult[0],function(err,result) {
		     		res.setHeader('Content-Type', 'application/json');
	    			res.send(JSON.stringify({ "status": "success","storyid":storyid}));
		     	});
	     	  	
			}
			else
			{
				//console.log("writerresult = "+JSON.stringify(writerresult));
				var storylistArry = writerresult[0].storylist;
				//var storylength = storylistArry.length + 1;
				var storylength = shortid.generate();
				var json;
				var storytype = req.session.category;
				console.log("selected stortype = "+storytype);

				var thisweekviews = 0;
				var lastweekviews = 0;
				var alltimeviews =  0;				

				json = {
					"title":req.body.title,
					"subheading":req.body.subheading,
					"bodycontent":req.body.bodycontent,
					"storypicture":imageid,
					"subbodycontent1":subbodycontent1,
					"subimage1":subimage1,
					"subimageurl1":subimageurl1,
					"subbodycontent2":subbodycontent2,
					"subimage2":subimage2,
					"subimageurl2":subimageurl2,
					"subbodycontent3":subbodycontent3,
					"subimage3":subimage3,
					"subimageurl3":subimageurl3,
					"subbodycontent4":subbodycontent4,
					"subimage4":subimage4,
					"subimageurl4":subimageurl4,
					"subbodycontent5":subbodycontent5,
					"subimage5":subimage5,
					"subimageurl5":subimageurl5,
					"subbodycontent6":subbodycontent6,
					"subimage6":subimage6,
					"subimageurl6":subimageurl6,
					"subbodycontent7":subbodycontent7,
					"subimage7":subimage7,
					"subimageurl7":subimageurl7,
					"subbodycontent8":subbodycontent8,
					"subimage8":subimage8,
					"subimageurl8":subimageurl8,
					"subbodycontent9":subbodycontent9,
					"subimage9":subimage9,
					"subimageurl9":subimageurl9,
					"subbodycontent10":subbodycontent10,
					"subimage10":subimage10,
					"subimageurl10":subimageurl10,
					"createddate":"",
					"modifieddate":"",
					"storystatus":"In progress",
					"storycategory":storytype,
					"storyid":storylength,
					"thisweekviews":thisweekviews,
					"lastweekviews":lastweekviews,
					"alltimeviews":alltimeviews
				};
				var i= 0;
				req.session.storyid = storylength;
	 			writerresult[0].storylist.push(json);
		     	mongoConn.storyColl.save(writerresult[0],function(err,result) {
	     			res.setHeader('Content-Type', 'application/json');
	    			res.send(JSON.stringify({ "status": "success","storyid":storylength}));
		     	});	     	  
    		}
		});
	}
}

exports.getAllStoryList = function(req,res)
{
	if(req.session.userid)
	{
		var userid = req.session.userid;
		console.log("!!!!!------GetAllstorylists calling ...");		
		mongoConn.storyColl.find({"_id":ObjectId(userid)}).toArray(function(err,writerresult) 
		{
			var profilepictures = writerresult[0].profilepicture;
			var profileid = profilepictures[profilepictures.length -1];
			var o_id = new mongo.ObjectID(profileid);
			console.log("!!!!!------GetAllstorylists - collection ");
			mongoConn.profilepictureColl.find({"_id":o_id}).toArray(function(err,profileimage) 
			{
				console.log("!!!!!------GetAllstorylists - "+profileimage);
				if(profileimage.length >= 1)
				{
					var path = profileimage[0].path;
					console.log("!!!!!------GetAllstorylists - singleprofileimage - "+path);
					writerresult[0].profilepicture = path.split("public/")[1];
				}		
				else
				{
					writerresult[0].profilepicture = 'img/pro-pic.png';
				}

				console.log("!!!!!------GetAllstorylists listcount = "+writerresult[0].storylist.length);

				i=0;
				async.eachSeries(writerresult[0].storylist, function (story, callback)
				{
					console.log("!!!!!------GetAllstorylists - storylists looping ...");
					var storybodycontent_text = story.bodycontent;
					storybodycontent_html  = converter.makeHtml(storybodycontent_text);	
					//console.log("!!!!!------GetAllstorylists - storylists = "+ storybodycontent_html);	
					if(storybodycontent_html != null)story.bodycontent = storybodycontent_html.substring(0,100);						
					i++;
					callback();
				});

				res.send(writerresult[0]);
			});
		});
	}
};

// index.js continued
exports.storysubcontentImageUpload = function(req,res)
{
	console.log(req.body); //form fields
	var hostaddress = "http://"+req.headers.host;
	console.log(req.file); 
	var listicleimage = req.file;
	var path = req.file.path;

	imageUrl = hostaddress+"/"+path.split("public/")[1];
	//imageUrl = 'https://www.apichangelog.com//static/img/logos/contentful-128.png';

	client.getSpace(space_id)
    .then((space) => {
        console.log("Space created = "+JSON.stringify(space));
        space.createAsset({
          fields: {
            title: {
                'en-US' : imageUrl.replace(/^.*[\\\/]/, '')
            },
            file: {
              'en-US': {
                 contentType: 'image/jpeg',
                 fileName: imageUrl.replace(/^.*[\\\/]/, ''),
                 upload: imageUrl
              }
            }
          }
        })
        .then(function(e) {         
            e.processForAllLocales().then(function(data) {
	           	data.publish()
	            return {sysid : data.sys.id, path : data.fields.field['en-US'].url, name: data.fields.field['en-US'].fileName};
            })
            console.log(JSON.stringify(e));
            var contentfulfileurl = e.fields.file['en-US'].upload;

            console.log("Asset id = "+e.sys.id);	   
            console.log("-----contentfulfileurl = "+contentfulfileurl);

            listicleimage.profilecontenfulid = e.sys.id;
			listicleimage.contentfulfileurl = contentfulfileurl;

            mongoConn.profilepictureColl.save(listicleimage,function(err,result) 
            {
            	console.log("-----Save imagecontent to mongodb = "+JSON.stringify(result));
				res.send(JSON.stringify({ "status": "success","imageid":e.sys.id ,"imageurl":contentfulfileurl}));
            });
    	}); 
    });
};

exports.picturefileUpload = function(req,res)
{
	console.log("-------------base64 image uploading-----------------");
	var base64Data = req.body.base64;

 	// Regular expression for image type:
    // This regular image extracts the "jpeg" from "image/jpeg"
    var imageTypeRegularExpression      = /\/(.*?)$/;      

    // Generate random string
    var crypto = require('crypto');
    var seed = crypto.randomBytes(20);
    var uniqueSHA1String  = crypto.createHash('sha1').update(seed).digest('hex');
    var matches = base64Data.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
	var response = {};

	if (matches.length !== 3) 
	{
		return new Error('Invalid input string');
	}

	response.type = matches[1];
	response.data = new Buffer(matches[2], 'base64');
    var imageBuffer = response;
    var userUploadedFeedMessagesLocation = './public/cropImgUploads/';
    var uniqueRandomImageName = 'image-' + uniqueSHA1String;

    // This variable is actually an array which has 5 values,
    // The [1] value is the real image extension
    var imageTypeDetected  = imageBuffer.type.match(imageTypeRegularExpression);
    var userUploadedImagePath  = userUploadedFeedMessagesLocation +"Crop_"+uniqueRandomImageName + '.' + imageTypeDetected[1];
    var filename = "Crop_"+uniqueRandomImageName + '.' + imageTypeDetected[1];
    // Save decoded binary image to disk
    try
    {
		//console.log(imageBuffer.data);
	  	require('fs').writeFile(userUploadedImagePath, imageBuffer.data, function(err) 
	  	{
	  		if(err)
	      	{
	      		console.log("Image saving error = "+ err);
	      	}
	      	else
      		{
				var requestfile = {
					"fieldname" : "profilepicturefile",
					"originalname" : uniqueRandomImageName,
					"encoding" : "7bit",
					"mimetype" : "image/jpeg",
					"destination" : userUploadedFeedMessagesLocation,
					"filename" : filename,
					"path" : userUploadedImagePath,
					"size" : 13582
				}
      		}
	        console.log('DEBUG - feed:message: Saved to disk image attached by user:', userUploadedImagePath);
    		mongoConn.profilepictureColl.insert(requestfile,function(err, result) 
			{
				res.send(JSON.stringify({ "status": "success","fileid":result.insertedIds[0],"filename":result.ops[0].filename }));
			});

	  	});
    }
    catch(error)
    {
        console.log('ERROR:', error);
    }
};

exports.normalFileUpload = function(req,res)
{
    upload(req,res,function(err) 
    {
        //console.log(req.body);
        var requstfiles = req.files[0];	
        var filename = requstfiles.filename;	

        if(err) 
        {
        	console.log(err);
            return res.end("Error uploading file.");
        }
        else
    	{    	
			res.send(JSON.stringify({ "status": "success","filename":filename }));
    	}
    });
};

exports.deleteArticle = function(req,res)
{
	console.log(JSON.stringify(req.body));
	console.log("Articleid = "+req.body.articleid);
	var articleid = req.body.articleid;
	if(req.session.userid)
	{
		var userid = req.session.userid;
		mongoConn.storyColl.find({"_id":ObjectId(userid)}).toArray(function(err,writerresult) 
		{
			console.log("Save Story  with articleid = "+req.body.articleid);

			var storylists = writerresult[0].storylist;		
			console.log(JSON.stringify(storylists))	

			if(req.body.hasOwnProperty('articleid') && req.body.articleid != "")
			{
				var i = 0;
				async.eachSeries(storylists, function (prime, callback) 
				{
					console.log("Single story = "+JSON.stringify(prime)+", "+i);
					if(prime != undefined)
					{
						if(prime.storyid == articleid )		
						{
							//delete writerresult[0].storylist[i];
				            writerresult[0].storylist.splice(i, 1);
				            mongoConn.storyColl.save(writerresult[0],function(err,result) 
				            {
		    					res.send(JSON.stringify({ "status": "success" }));
				            });
						}		
					}					
					i++;			
					callback();
				});
	     		
			}
		});
	}
};

exports.getCityNames = function(req,res)
{
	var cities_dropdown = [];
	client.getSpace(space_id)
	.then((cityspace) => 
	{
		console.log("Get Cities space successfully = "+JSON.stringify(cityspace));

		cityspace.getEntries({content_type: 'city'})
		.then(function(cities) 
		{
			console.log("total cities = "+typeof cities.items);
			console.log(JSON.stringify(cities.items));
			res.send(cities.items);		
		})
	})
};

exports.getStory = function(req,res)
{
	if(req.session.storyid && req.session.userid)
	{
		var userid = req.session.userid;

		mongoConn.storyColl.find( {"_id":ObjectId(userid)}).toArray(function(err,articleresult)
		{
			//	console.log(articleresult[0]);
			var storyid = req.session.storyid;
			console.log("---storyid = "+storyid)
			//var story = articleresult[0].storylist[storyid-1];
			//console.log("Story lists = "+JSON.stringify(articleresult[0].storylist))
		
			var storyval = _.filter(articleresult[0].storylist, function(q){return q.storyid == storyid;});
			var story = storyval[0];
			res.send(story);			  					
		});
	}
};
