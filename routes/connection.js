// Retrieve
var config = require('config');
var MONGODB_URI = process.env.MONGODB_URI;

var MongoClient = require('mongodb').MongoClient;


// mongodump -h ds141685-a0.mlab.com:41685,ds141685-a1.mlab.com:41685 -u heroku_g9lw5ts9 -p jlv5d9kvfgbfq9k8jlua26mp07 -d heroku_g9lw5ts9 -o /home/devi/Subbiah/
// mongo ds141685-a0.mlab.com:41685/heroku_g9lw5ts9 -u heroku_g9lw5ts9 -p jlv5d9kvfgbfq9k8jlua26mp07 // mongo connection from commandline
// mongorestore /home/devi/Subbiah/unipapermongoDb_dump/heroku_g9lw5ts9    // Mongo DB Restore from dumped database

// Connect to the db
MongoClient.connect(MONGODB_URI, function(err, db)
{
	if(err) { return console.dir(err); }

	console.log("MongoDB connnection established");

	db.collection('writerscollection', {w:1}, function(err, collection) 
	{
		exports.storyColl = collection;
	});

	db.collection('profilepicturecollection', {w:1}, function(err, collection) 
	{
		exports.profilepictureColl = collection;
	});

 	db.collection('dailyviewcollection', {w:1}, function(err, collection)
        {
                exports.dailyviewCollection = collection;
        });

	db.collection('writerscollection').createIndex( { "$**": "text" });
});
