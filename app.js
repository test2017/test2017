/**
 * Module dependencies.
 */
// Application Configuration Details
var config = require('config');
var contentfulConfig = config.get('Unipaperwriterportal.contentfulConfig');
var mailchimpConfig = config.get('Unipaperwriterportal.mailchimpConfig');
var serverConfig = config.get('Unipaperwriterportal.serverConfig');
var uploadimageDirConfig = config.get('Unipaperwriterportal.uploadimageDirConfig');

var express = require('express');
_ = require('underscore')._;
var ObjectId = require('mongodb').ObjectID;
var multer = require('multer');
var mongo = require('mongodb');
var async = require('async');
var mcapi = require('mailchimp-api');

mc = new mcapi.Mailchimp(mailchimpConfig.accessid);

var contentful = require('contentful-management'); 

var space_id =  contentfulConfig.spacieid;
var accessToken = contentfulConfig.accesstoken;
var client = contentful.createClient({
  // This is the access token for this space. Normally you get both ID and the token in the Contentful web app 
  accessToken: accessToken
})

//var bugsnag = require("bugsnag");
//bugsnag.register("dee47e560886e5df8b9aaf77aafa0996");
//bugsnag.notify(new Error("Non-fatal"));

var mongoConn = require('./routes/connection');
var mongo = require('mongodb');
var async = require('async');
var shortid = require('shortid');


createAssetid = function(profilepictureid,hostaddress,callback)
{
    var o_id = new mongo.ObjectID(profilepictureid);
    mongoConn.profilepictureColl.find({"_id":o_id}).toArray(function(err,profileimage)
    {
        console.log("!!!!! ----- CreateAssetid - profileimage ="+profileimage);
        var profilecontenfulid = '';

        if(profileimage.length >= 1)
        {
            var path = profileimage[0].path;
            imageUrl = hostaddress+"/"+path.split("public/")[1];

            if(profileimage[0].profilecontenfulid != undefined)
            {
                profilecontenfulid = profileimage[0].profilecontenfulid;
            }
        }
        else
        {
            imageUrl = hostaddress+"/img/pro-pic.png";
        }

        //imageUrl = "http://unipaper-writers.herokuapp.com/img/unipaperlogo.png";

        if(profilecontenfulid != '')
        {
            callback(profilecontenfulid);
        }
        else
        {
            client.getSpace(space_id)
            .then((space) => {
                //console.log("Space created = "+JSON.stringify(space));
                space.createAsset({
                  fields: {
                    title: {
                        'en-US' : imageUrl.replace(/^.*[\\\/]/, '')
                    },
                    file: {
                      'en-US': {
                         contentType: 'image/jpeg',
                         fileName: imageUrl.replace(/^.*[\\\/]/, ''),
                         upload: imageUrl
                      }
                    }
                  }
                })
                .then(function(e) {

                    e.processForAllLocales().then(function(data) {  
                        console.log("!!!!! ----- CreateAssetid -  afterprocessForLocales "+JSON.stringify(data));
                        data.publish()                  
                        console.log("!!!!! ----- CreateAssetid -  contentful image url = "+data.fields.file['en-US'].url);  

                        console.log("!!!!! ----- CreateAssetid - final created asset - "+JSON.stringify(e));

                        console.log("!!!!! ----- CreateAssetid - final sysid "+e.sys.id);

                        if(profileimage.length >= 1)
                        {
                            profileimage[0].profilecontenfulid = e.sys.id;
                            profileimage[0].profilecontenfulimageurl = data.fields.file['en-US'] != undefined ? data.fields.file['en-US'].url : '';
                            mongoConn.profilepictureColl.save(profileimage[0],function(err,result) {});
                            callback(e.sys.id);
                        }
                        else
                        {
                            callback(e.sys.id);
                        }
                        //return {sysid : data.sys.id, path : data.fields.field['en-US'].url, name: data.fields.field['en-US'].fileName};
                    })                    
                });
            });
        }
    });
}

getRandomInt = function(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

getWriterImageurl = function(imageid,callback)
{
    //console.log("contentful id = "+imageid);
    var o_id = new mongo.ObjectID(imageid);
    mongoConn.profilepictureColl.find({"_id":o_id}).toArray(function(err,profileimage) 
    {
        //console.log(profileimage);
        var profileimageurl = '';
        if(profileimage.length >= 1)
        {
            var path = profileimage[0].path;
            profileimageurl = "./"+path.split("public/")[1];
        }
        else
        {
            profileimageurl = "./img/pro-pic.png";
        }
        console.log(profileimageurl);
        callback(profileimageurl);
    });
}

getNotificationsCount = function(req,res,notifyCallback)
{
    if(req.session.userid)
    {   
        var userid = req.session.userid;
        mongoConn.storyColl.find({"_id": ObjectId(userid)}).toArray(function(err, document) 
        {
            if(!err)
            {
                var notificationcount = 0;
                if(document[0].notification != undefined)
                {
                    var notificationList = document[0].notification;
                    var i=0;
                    async.eachSeries(notificationList, function (notification, callback) 
                    {
                        if(notification.notificationstatus == '')i++;
                        callback();
                    });
                    notificationcount = i;
                }
                else
                {
                    notificationcount = 0;
                }
                notifyCallback(notificationcount);               
            }
        });         
    }
    else
    {
        res.redirect('/');
    }
}

var storage = multer.diskStorage({
    destination: function(req, file, cb) 
    {
        cb(null, uploadimageDirConfig.uploadimageDummy)
    },
    filename: function(req, file, cb) 
    {
        var originalfilename = file.originalname;
        var filename = originalfilename.substr(0, originalfilename.lastIndexOf('.'));
        var extension = originalfilename.split('.').pop();

        cb(null, filename + '-' + Date.now() + '.' + extension)
    }
});


var listiclestorage = multer.diskStorage({
    destination: function(req, file, cb) 
    {
        cb(null, uploadimageDirConfig.uploadimageDir)
    },
    filename: function(req, file, cb) 
    {
        var originalfilename = file.originalname;
        var filename = originalfilename.substr(0, originalfilename.lastIndexOf('.'));
        var extension = originalfilename.split('.').pop();

        cb(null, filename + '-' + Date.now() + '.' + extension)
    }
});

GLOBAL.upload = multer({ storage: storage }).array('profilepicturefile', 1);

var http = require('http');
var path = require('path');


var app = express();

// all environments app.set('port', process.env.PORT || 3000);
app.set('port', process.env.PORT || serverConfig.port);

//app.set('views', path.join(__dirname, 'views'));
app.use(express.static(path.join(__dirname, 'public')));
//app.use(express.static(path.join(__dirname, 'uploads')));
app.use(express.static('node_modules'));

app.set('view engine', 'ejs');
//app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());

app.use(express.cookieParser());
app.use(express.session({ secret: '1234567890QWERTY' }));
app.use(app.router);

var routes = require('./routes');
var story = require('./routes/story');

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

app.get('/', routes.index);
//app.get('/uploads/*.(png|jpeg|jpg|gif)', routes.uploadimages);

app.get('/storyview', story.view);
app.get('/storytypeview', story.storytypeview);
app.get('/storypreview', story.storypreview);

app.get('/storyboardview/:storytype', story.storyboardview);

app.post('/writerstory', story.savewriterStory);

app.post('/saveListicleStory',story.saveListicleStory);

app.post('/saveGalleryStory',story.saveGalleryStory);

app.post('/saveProfilePicture', story.picturefileUpload);

app.post('/normalFileUpload', story.normalFileUpload);

app.post('/storysubcontentImageUpload',  multer({ storage: listiclestorage }).single('listiclepicture'), story.storysubcontentImageUpload   );

app.get('/getAllStories', story.getAllStoryList);

app.post('/savesignupform', routes.saveSignupForm);
app.post('/sigininAction', routes.sigininAction);
app.post('/sigininSocialAction', routes.sigininSocialAction);
app.get('/profileview', routes.profileview);

app.get('/getProfileupdateStatus', routes.getProfileupdateStatus);

app.get('/logout', routes.logoutWriter);

app.post('/submitArticletoContentful', routes.submitArticletoContentful);

app.post('/profileformSubmit', routes.profileformSubmit);

app.post('/updateCategory', routes.updateCategory);

app.post('/getSearchStories', routes.getSearchStories);

app.post('/deleteArticle', story.deleteArticle);

app.get('/getCityNames', story.getCityNames);

app.get('/getStory', story.getStory);

app.get('/notification',routes.notification);

app.get('/getNotifications',routes.getNotifications);

app.post('/updateNotification',routes.updateNotification);

app.post('/deleteNotification',routes.deleteNotification);

app.get('/performance', routes.performance);


// Webhook integration 
var bodyParser = require("body-parser"),
    methodOverride = require("method-override");

app.use(function(req, res, next) {
    if (req.headers['content-type'] === 'application/vnd.contentful.management.v1+json') req.headers['content-type'] = 'application/json';
    next();
});

//app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(methodOverride("X-HTTP-Method-Override"));

app.post('/contentfulWebhook', function(req, res) {
    //console.log("Incoming request");
    //console.log(JSON.stringify(req.body));
    var prime = req.body;
    if (prime.fields.writer != undefined && (prime.sys.contentType.sys.id == "writer" || prime.sys.contentType.sys.id == "article" || prime.sys.contentType.sys.id == "gallery" || prime.sys.contentType.sys.id == "listicle")) {

        var title = '';
        var subheading = '';
        var bodycontent = '';
        var storystatus = '';
        var note = '';

        if (prime.fields.title != undefined) title = prime.fields.title["en-US"]
        if (prime.fields.subHeading != undefined) subheading = prime.fields.subHeading["en-US"];
        if (prime.fields.content != undefined) bodycontent = prime.fields.contentBody["en-US"];
        if (prime.fields.status != undefined && prime.fields.status["en-US"] != undefined) storystatus = prime.fields.status["en-US"];
        if (prime.fields.note != undefined) note = prime.fields.note["en-US"];

        //console.log("Writer id = "+prime.fields.writer["en-US"].sys.id);
        var writerid = prime.fields.writer["en-US"].sys.id;
        mongoConn.storyColl.find({ writerid: writerid }).toArray(function(err, document) {
            //console.log("Document value = "+JSON.stringify(document));

            if (document[0] != undefined) {
                //console.log("--------------------------------------------start-------------------------------------------------");
                //console.log(JSON.stringify(prime));
                //console.log("Writer id = "+prime.fields.writer["en-US"].sys.id);
                //console.log("Articleid = "+prime.sys.id);
                var articleid = prime.sys.id;
                var firstpublish = prime.sys.firstPublishedAt
                //console.log("writerid =  "+prime.fields.writer["en-US"].sys.id);
                //console.log("=================="+JSON.stringify(document[0]));
                //console.log(JSON.stringify(document[0].storylist));

                async.eachSeries(document[0].storylist, function(story, innercallback) {
                    if (story.articleid == articleid) {
                        //story.title = title;
                        //story.subheading = subheading
                        //story.bodycontent = bodycontent;
                        if((prime.sys.firstPublishedAt != undefined && prime.sys.firstPublishedAt != '') || (prime.sys.revision != undefined && prime.sys.revision > 0)) storystatus = "Published";
                        if (storystatus != '') {
                            story.storystatus = storystatus;
                            story.note = note;
                        }
                    }
                    innercallback();
                });
                mongoConn.storyColl.save(document[0], function(err, result) {});

                //console.log("-------------------------------------------end------------------------------------------------------");

            }

        });
    }
    else if(prime.sys.contentType.sys.id == "notification")
    {
        console.log("-----------------Notitficationid = "+prime.sys.contentType.sys.id);
        //console.log(JSON.stringify(prime));
        //var writerid = prime.fields.city["en-US"].sys.id;
        console.log("-----------------Notification started");
        console.log(JSON.stringify(prime));
        
        console.log("-----------------Notification = "+JSON.stringify(prime.fields.city['en-US']));

        var notifyid = '';
        var createdAt = '';
        var subject = '';
        var messagebody = '';
        var city = '';

        if(prime.sys.id != undefined) notifyid = prime.sys.id;
        if(prime.sys.createdAt != undefined) createdAt = prime.sys.createdAt;
        if(prime.fields.subject['en-US']!= undefined) subject = prime.fields.subject['en-US'];
        if(prime.fields.messagebody['en-US'] != undefined) messagebody = prime.fields.messagebody['en-US'];
        if(prime.fields.city['en-US'] != undefined) city = prime.fields.city['en-US'];

        console.log("-----------------Notification notifyid = "+notifyid);
        console.log("-----------------Notification createdat = "+createdAt);
        console.log("-----------------Notification subject = "+subject);
        console.log("-----------------Notification messagebody = "+messagebody);
        console.log("-----------------Notification city = "+city);



        mongoConn.storyColl.find({ city:city}).toArray(function(err, document) 
        {
            if(document[0] != undefined || document.length > 0)
            {
                async.eachSeries(document, function (writerData, innercallback) 
                {
                    console.log("Comming city = "+city+", "+prime.fields.city['en-US']);

                    if(writerData.city == city || city == "National")
                    {
                        var notificatData = { 
                            notifyid:notifyid,
                            subject:subject,
                            messagebody:messagebody,
                            city:city,
                            notificationstatus:"",
                            createdAt:createdAt
                        };  

                        //console.log("-----------------Notification before notification checking = "+writerData.notification);
                        if(writerData.notification == undefined)writerData.notification = [];
                        //console.log("-----------------Notification After notification checking = "+writerData.notification +", "+writerData.notification.length);
                        if(writerData.notification.length == 0)writerData.notification.push(notificatData);
                        else
                        {
                            var i = 0;
                            async.eachSeries(writerData.notification, function (writernotificationData, innerlevelcallback) 
                            {
                                if(writernotificationData.notifyid == notifyid)
                                {
                                    writernotificationData.subject = subject;
                                    writernotificationData.messagebody = messagebody;
                                    writernotificationData.city = city;
                                    writernotificationData.notificationstatus = "";
                                    writernotificationData.createdAt = createdAt;
                                    i++;
                                }                                           
                                innerlevelcallback();
                            });     

                            if(i == 0)writerData.notification.push(notificatData);
                        }           
                                    
                        mongoConn.storyColl.save(writerData,function(err,result) {});
                    }
                    innercallback();
                });

                callback(); // Alternatively: callback(new Error());
            }
            else
            {
                if(city == "National")
                {
                    mongoConn.storyColl.find().toArray(function(err, document) 
                    {
                        if(document[0] != undefined || document.length > 0)
                        {
                            async.eachSeries(document, function (writerData, innercallback) 
                            {
                                console.log("-----------------Comming city = "+city);
                                
                                    var notificatData = { 
                                        notifyid:notifyid,
                                        subject:subject,
                                        messagebody:messagebody,
                                        city:city,
                                        notificationstatus:"",
                                        createdAt:createdAt
                                    };  

                                    //console.log("-----------------Notification before notification checking = "+writerData.notification);
                                    if(writerData.notification == undefined)writerData.notification = [];
                                    //console.log("-----------------Notification After notification checking = "+writerData.notification +", "+writerData.notification.length);
                                    if(writerData.notification.length == 0)writerData.notification.push(notificatData);
                                    else
                                    {
                                        var i = 0;
                                        async.eachSeries(writerData.notification, function (writernotificationData, innerlevelcallback) 
                                        {
                                            if(writernotificationData.notifyid == notifyid)
                                            {
                                                writernotificationData.subject = subject;
                                                writernotificationData.messagebody = messagebody;
                                                writernotificationData.city = city;
                                                writernotificationData.notificationstatus = "";
                                                writernotificationData.createdAt = createdAt;
                                                i++;
                                            }                                           
                                            innerlevelcallback();
                                        });     

                                        if(i == 0)writerData.notification.push(notificatData);
                                    }           
                                                
                                    mongoConn.storyColl.save(writerData,function(err,result) 
                                    {
                                        innercallback();
                                    });
                            });

                            callback(); // Alternatively: callback(new Error());
                        }
                        else
                        {
                            console.log("--------------------------------------------------------------else condition");
                            callback(); // Alternatively: callback(new Error());            
                        }                   
                    });                                     
                }
                else
                {
                    callback();
                }                       
            }                   

        });     
    }
   

})

http.createServer(app).listen(app.get('port'), function() {
    console.log('Express server listening on port ' + app.get('port'));
});
