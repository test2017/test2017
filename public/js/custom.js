

var unipaperapp = angular.module('unipaperApp',['ngRoute','angularMoment','ngSanitize']);
var simplemde = null;

unipaperapp.controller("unipaperCtrl", ['$scope','writerService','$window',function($scope,writerService,$window) 
{

  $scope.croppietitle = '';
  $scope.croppietitle = "Create article image";

  var myEl = angular.element( document.querySelector( '#storylistview' ) );
  var profileview = angular.element( document.querySelector( '#profileview') );
  var storyboardtrue = angular.element( document.querySelector( '#textareaSimpleMDE') );
  var notificationview = angular.element( document.querySelector( '#notificationview') );
  var storyboardlisticleview = angular.element( document.querySelector( '#storyboardlisticle'));
  var storyboardgalleryview = angular.element( document.querySelector( '#storyboardgallery'));

  $scope.storylists = $window.storylists;
  $scope.thisweekviewsWritersCount = $window.thisweekviewsWritersCount;
  $scope.lastweekviewsWritersCount = $window.lastweekviewsWritersCount;
  $scope.alltimeviewsWritersCount = $window.alltimeviewsWritersCount;

  $scope.myarticle_alltimeviews = $window.myarticle_alltimeviews
  $scope.myarticle_thisweekviews = $window.myarticle_thisweekviews
  $scope.myarticle_lastweekviews = $window.myarticle_lastweekviews

  var imageCroppie = null;

  if($window.notificationcount > 0)
  {
    $scope.notificationcount = $window.notificationcount; 
    $scope.notificationclass = 'label-success';
  }  
  else
  {
    $scope.notificationclass = ''; 
  }
 
  $scope.substoryes = $window.substoryes;

  if(profileview.length)
  {   
    $scope.imageUrl = 'img/pro-pic.png';
  }
  else $scope.imageUrl = '../img/pro-pic.png';

  $scope.showControls = true;
  $scope.fit = false;

  $scope.myButtonLabels = {
    rotateLeft: ' Rotate Left',
    rotateRight: ' Rotate Right',
    zoomIn: ' Zoom In ',
    zoomOut: ' Zoom Out ',
    fit: ' Fit',
    crop: 'Upload'
  };    

/*
  $scope.getCropperApi = function(api) {
   api.zoomIn(3);
    api.zoomOut(2);
    api.rotate(0);
    //api.applyRotation(90);
    api.fit();
    //$scope.resultImage = api.crop();
  };
*/
  $scope.whatClassIsIt= function(someValue)
  {
    if(someValue != "")
      return "fa-paperclip"; 
    else
      return "";
  }

  $scope.whatClassIsIt_rotate = function(someValue)
  {    
     if(someValue != "" && someValue!= undefined)
      return "fa-refresh util-spin";
    else
      return "";
  }

  $scope.whatngStyle= function(someValue,someValue1)
  {
    if(someValue != "" || someValue1 != '')
    {
      myObj = {"display":"block"}
      return myObj;
    }
    else
      return "";
  }


  if(storyboardlisticleview.length == 1)
  {
    $scope.croppietitle = "Create listicle image";
    $scope.listiclelists = [];
    if($scope.listiclelists.length == 0)
    {
      $scope.listiclelists.push({"title":"","bodycontent":""});  
    }

    $scope.addListicle = function()
    {
        $scope.listiclelists.push({"title":"","bodycontent":""});  
    }
  }

  if(storyboardgalleryview.length == 1)
  {
    $scope.croppietitle = "Create gallery image";
  }


  if(storyboardtrue.length == 1)
  {
    simplemde = new SimpleMDE({
      element: document.getElementById("textareaSimpleMDE"),
      spellChecker: false,
      toolbar: ["bold", "italic", "heading", "|", "quote", "unordered-list","ordered-list"],
      previewRender: function(plainText) {
        //var html = customMarkdownParser(plainText); // Returns HTML from a custom parser
        //alert(html);
      },
    });

    simplemde.codemirror.on("change", function()
    {
      //console.log(simplemde.value());
      $scope.writerarticle.bodycontent = simplemde.value();
      $scope.listiclearticle.bodycontent = simplemde.value();
      $scope.galleryarticle.bodycontent = simplemde.value();
    });  

    writerService.getStory().then(function(response) 
    { 
      //alert(JSON.stringify(response));
      simplemde.value(response.bodycontent);
    });
  }

  if(profileview.length)
  {
      $scope.croppietitle = "Create profile picture";
      $scope.imagecrop = "false";
      writerService.getCityNames($scope.writerarticle).then(function(response) 
      { 
        cities_dropdown = [];
        response.forEach(function(city)
        { 

		     if(city.fields.name != undefined && city.fields.name['en-US'] != "National" )
          {
            cities_dropdown.push({name:city.fields.name['en-US']});
          }
         
        });

        $scope.citynames = cities_dropdown
    });
  }
  if(notificationview.length == 1)
  {
    writerService.getNotifications().then(function(response) 
    { 
     
      if(response.length > 0)
      {
        $scope.notifications = response;
        $scope.startIndex = 0;
        $scope.notificationDelete = "Delete";
        $scope.notificationIndex = $scope.startIndex;
        $scope.notificationBody = $scope.notifications[$scope.startIndex].messagebody;    
        $scope.notificationSubject = $scope.notifications[$scope.startIndex].subject;
        $scope.notificationIdvalue = $scope.notifications[$scope.startIndex].notifyid;    
        $scope.notificationValue = $scope.startIndex;
      }

    });

    $scope.getNotification = function(index)
    {
      $scope.startIndex = index;
      $scope.notificationBody = $scope.notifications[$scope.startIndex].messagebody;  
      $scope.notificationSubject = $scope.notifications[$scope.startIndex].subject;
      $scope.notificationIdvalue = $scope.notifications[$scope.startIndex].notifyid;
      $scope.notificationValue = $scope.startIndex;
      $scope.notificationDelete = "Delete";

    }

    $scope.updateNotification = function(id,startIndex)
    {     
      writerService.updateNotification(id).then(function(response) 
      {        
        $scope.notifications[startIndex].notificationstatus = "note-unread";
        if( ($scope.notificationcount - 1) > 0)
        {
          $scope.notificationcount = $scope.notificationcount - 1;
          $scope.notificationclass = 'label-success';

        }
        else
        {
          $scope.notificationcount = '';
          $scope.notificationclass = '';
        }
      });
    }

    $scope.deleteNotifications = function(notifyid,notifyindex)
    {
      if(notifyid != '' && notifyindex >= 0)
      {
        writerService.deleteNotification(notifyid).then(function(response) 
        {  
          $scope.notifications.splice(notifyindex,1);
          $scope.notificationcount = '';
          $scope.notificationclass = '';
          $scope.notificationIdvalue = '';
          $scope.notificationValue = '';
          $scope.notificationDelete = '';
          $scope.notificationBody =''; 
          $scope.notificationSubject = '';
        });
      }   
    }
  }

  if(myEl.length == 1)
  {    

    //writerService.("searchtext").then(function(response)
    //{
    //  console.log(response);
    //});

    writerService.getStoryList($scope.writerarticle).then(function(response) 
    { 
      //alert(JSON.stringify(response))
      var storylistsarr = response.storylist; 
      for(i=0;i<storylistsarr.length;i++)
      {
        if(storylistsarr[i].storystatus == 'In progress')
        {
          storylistsarr[i].storypicture = 'img/in-progress.png';
        }
        else if(storylistsarr[i].storystatus == 'Submited')
        {
          storylistsarr[i].storypicture = 'img/submited.png';
        }
        else if(storylistsarr[i].storystatus == 'Review')
        {
          storylistsarr[i].storypicture = 'img/review.png';
        }
        else if(storylistsarr[i].storystatus == 'Published')
        {
          storylistsarr[i].storypicture = 'img/published.png';
        }
        else if(storylistsarr[i].storystatus == 'Rejected')
        {
          storylistsarr[i].storypicture = 'img/reject.png';
        }
        else
        {
          storylistsarr[i].storypicture = 'img/in-progress.png';
        }
      }
      $scope.storylists =   storylistsarr;

      $scope.profilepicture = response.profilepicture;
      var profileupdate = response.profileupdate;

      if(profileupdate == "false")
      {
        $scope.showModal = !$scope.showModal;
      }
    });
  }

	$scope.writeStoryFunc = function()	
  {
    writerService.getProfileupdateStatus("writerstory").then(function(response) 
    {
      if(response.profileupdate == "false" || response.profileupdate == false)
      {
        //alert("Please update your profile");
        return;
      }
      else
      {
        window.location.href = '/storytypeview';
      }
    });
	};	

  $scope.storytypenext = function()
  {
    var storytype = $scope.selectedCat;
    if(storytype == undefined)
    {
      alert("Please select story type");
    }
    else
    {
      window.location.href = '/storyboardview/'+storytype;
    }
  };

  $scope.storytypeback = function(type)
  {
    window.location.href = '/storyview';
  };

  $scope.storypreviewnext = function()
  {
    window.location.href = '/storypreview';
  };

  $scope.selectCat = function(event)
  {
    writerService.updateCategory($(event.target).attr("storytype")).then(function(response) 
    {
      if(response.status == "success")
      {
        $scope.selectedCat = response.storytype;
        window.location.href = '/storyboardview/'+$scope.selectedCat;
      }   
    });
  };

  $scope.mySelectedArticle = function(event)
  { 
    console.log( $(event.target));
    console.log($(event.target).parents('a').attr("storycategory"));
    var category = $(event.target).parents('a').attr("storycategory");
    var storyid = $(event.target).parents('a').attr("storyid");
    var storystatus = $(event.target).parents('a').attr("storystatus");
    var articlestatus = $(event.target).parents('a').attr("articlestatus");
    if(articlestatus != "Submited" && articlestatus != "Rejected" && articlestatus != "Published")
    {
      window.location.href = "/storyboardview/"+category+"?storyid="+storyid+"&storystatus=edit";
    }   
  };

  $scope.showUpdateProfileForm = function()
  {
    window.location.href = '/profileview';
  };

  $scope.profileview = function()
  {
    window.location.href = '/profileview';
  };
  $scope.writerarticle = {};

	$scope.writerArticleformsubmit = function() 
  {
    $scope.writerarticle.imageid = $scope.imageid;
    $scope.writerarticle.titleerr = "";
    $scope.writerarticle.bodycontenterr = "";

    //alert($scope.writerarticle.bodycontent);
    
		if($scope.writerarticle.title != "" &&  $scope.writerarticle.bodycontent != "")
    {
  		writerService.saveStory($scope.writerarticle).then(function(response) 
      {
        //alert(JSON.stringify("Article has been saved"));
        $scope.writerarticle.storyid = response.storyid;
        window.location.href = '/storypreview';    
      });
    }
    else
    {
      if($scope.writerarticle.title == "") $scope.writerarticle.titleerr = "form-group has-danger";
      if($scope.writerarticle.bodycontent == "") $scope.writerarticle.bodycontenterr = "form-group has-danger";
      return;     
    }
	};

  $scope.listiclearticle = {};
  $scope.writerListicleformsubmit  = function() 
  {
    $scope.listiclearticle.imageid = $scope.imageid;
    $scope.listiclearticle.titleerr = "";
    $scope.listiclearticle.bodycontenterr = "";    
    if($scope.listiclearticle.title != "" &&  $scope.listiclearticle.bodycontent != "")
    {      
      writerService.saveListicleStory($scope.listiclearticle).then(function(response) 
      {
        //alert(JSON.stringify("Article has been saved"));
        $scope.writerarticle.storyid = response.storyid;
        window.location.href = '/storypreview';    
      });      
    }
    else
    {
      if($scope.listiclearticle.title == "") $scope.listiclearticle.titleerr = "form-group has-danger";
      if($scope.listiclearticle.bodycontent == "" || $scope.listiclearticle.bodycontent == undefined) $scope.listiclearticle.bodycontenterr = "form-group has-danger";
      return;     
    }
  };


  $scope.galleryarticle = {};
  $scope.writerGalleryformsubmit  = function() 
  {
    $scope.galleryarticle.imageid = $scope.imageid;
    $scope.galleryarticle.titleerr = "";
    $scope.galleryarticle.bodycontenterr = "";    
    if($scope.galleryarticle.title != "" &&  $scope.galleryarticle.bodycontent != "")
    {      
      writerService.saveGalleryStory($scope.galleryarticle).then(function(response) 
      {
        //alert(JSON.stringify("Article has been saved"));
        $scope.galleryarticle.storyid = response.storyid;
        window.location.href = '/storypreview';    
      });      
    }
    else
    {
      if($scope.galleryarticle.title == "") $scope.galleryarticle.titleerr = "form-group has-danger";
      if($scope.galleryarticle.bodycontent == "" || $scope.galleryarticle.bodycontent == undefined) $scope.galleryarticle.bodycontenterr = "form-group has-danger";
      return;     
    }
  };


	$scope.uploadFile = function() 
  {

    $scope.showImageCropModal = !$scope.showImageCropModal;    
    	var fd = new FormData();
    	//Take the first selected file
    	fd.append("profilepicturefile", files[0]);
    	writerService.saveFile(fd).then(function(response) {
    		//alert("file uploaed successfully "+JSON.stringify(response));
        //$scope.imageid = response.fileid;
        //$scope.filename = response.filename;
        //$scope.picturename = '../uploads/'+response.filename;
        //$scope.picturename = '../cropImgUploads/'+response.filename;    
        //alert("------------"+$scope.showImageCropModal);
        $scope.showImageCropModal = !$scope.showImageCropModal;
    	});      
	}

  $scope.normalFileUpload = function(files)
  {
    var fd = new FormData();
    //Take the first selected file
    fd.append("profilepicturefile", files[0]);
    writerService.normalFileUpload(fd).then(function(response) 
    {
      $scope.imageUrl = 'uploads/dummyimages/'+response.filename;   
      $scope.showImageCropModal = !$scope.showImageCropModal;
      var el = document.getElementById('imagecrop-basic');
      $(el).html('');
      imageCroppie = new Croppie(el, {
      viewport: { width: 150, height: 200 },
      boundary: { width: 300, height: 300 },
      //showZoomer: false,
      //enableOrientation: true
      });
      ///alert($scope.imageUrl);
      imageCroppie.bind({
      url: $scope.imageUrl,
      points: [77,469,280,739],
      orientation: 4
      });
    }); 
  }

  $scope.galleryFileUpload = function(files,id)
  {

    if(parseInt(id) == 1)$scope.image_rotate1 = "show";
    if(parseInt(id) == 2)$scope.image_rotate2 = "show";    
    if(parseInt(id) == 3)$scope.image_rotate3 = "show";
    if(parseInt(id) == 4)$scope.image_rotate4 = "show";
    if(parseInt(id) == 5)$scope.image_rotate5 = "show";
    if(parseInt(id) == 6)$scope.image_rotate6 = "show";
    if(parseInt(id) == 7)$scope.image_rotate7 = "show";
    if(parseInt(id) == 8)$scope.image_rotate8 = "show";
    if(parseInt(id) == 9)$scope.image_rotate9 = "show";
    if(parseInt(id) == 10)$scope.image_rotate10 = "show";

    var fd = new FormData();
    //Take the first selected file
    fd.append("listiclepicture", files[0]);
    writerService.storysubcontentImageUpload(fd).then(function(response) 
    {
      //$scope.imageUrl = '../uploads/'+response.filename;  
      //alert(JSON.stringify(response));
      if(parseInt(id) == 1)
      {
        $scope.galleryarticle.subimage1 = response.imageid;
        $scope.galleryarticle.subimageurl1= response.imageurl;
        $scope.image1 = "show";
        $scope.image_rotate1 = '';
      }
      if(parseInt(id) == 2)
      {
        $scope.galleryarticle.subimage2 = response.imageid;
        $scope.galleryarticle.subimageurl2 = response.imageurl;
        $scope.image2 = "show";
        $scope.image_rotate2 = '';

      }
      if(parseInt(id) == 3)
      {
        $scope.galleryarticle.subimage3 = response.imageid;
        $scope.galleryarticle.subimageurl3 = response.imageurl;
        $scope.image3 = "show";
        $scope.image_rotate3 = '';
      }
      if(parseInt(id) == 4)
      {
        $scope.galleryarticle.subimage4 = response.imageid;
        $scope.galleryarticle.subimageurl4 = response.imageurl;
        $scope.image4 = "show";
        $scope.image_rotate4 = '';
      }
      if(parseInt(id) == 5)
      {
        $scope.galleryarticle.subimage5 = response.imageid;
        $scope.galleryarticle.subimageurl5 = response.imageurl;
        $scope.image5 = "show";
        $scope.image_rotate5 = '';
      }
      if(parseInt(id) == 6)
      {
        $scope.galleryarticle.subimage6 = response.imageid;
        $scope.galleryarticle.subimageurl6 = response.imageurl;
        $scope.image6 = "show";
        $scope.image_rotate6 = '';
      }
      if(parseInt(id) == 7)
      {
        $scope.galleryarticle.subimage7 = response.imageid;
        $scope.galleryarticle.subimageurl7 = response.imageurl;
        $scope.image7 = "show";
        $scope.image_rotate7 = '';
      }
      if(parseInt(id) == 8)
      {
        $scope.galleryarticle.subimage8 = response.imageid;
        $scope.galleryarticle.subimageurl8 = response.imageurl;
        $scope.image8 = "show";
        $scope.image_rotate8 = '';
      }
      if(parseInt(id) == 9)
      {
        $scope.galleryarticle.subimage9 = response.imageid;
        $scope.galleryarticle.subimageurl9 = response.imageurl;
        $scope.image9 = "show";
        $scope.image_rotate9 = '';
      }
      if(parseInt(id) == 10)
      {
        $scope.galleryarticle.subimage10 = response.imageid;
        $scope.galleryarticle.subimageurl10 = response.imageurl;      
        $scope.image10 = "show";
        $scope.image_rotate10 = '';
      }
    }); 
  }

  $scope.listicleFileUpload = function(files,id)
  {
    if(parseInt(id) == 1)$scope.image_rotate1 = "show";
    if(parseInt(id) == 2)$scope.image_rotate2 = "show";    
    if(parseInt(id) == 3)$scope.image_rotate3 = "show";
    if(parseInt(id) == 4)$scope.image_rotate4 = "show";
    if(parseInt(id) == 5)$scope.image_rotate5 = "show";
    if(parseInt(id) == 6)$scope.image_rotate6 = "show";
    if(parseInt(id) == 7)$scope.image_rotate7 = "show";
    if(parseInt(id) == 8)$scope.image_rotate8 = "show";
    if(parseInt(id) == 9)$scope.image_rotate9 = "show";
    if(parseInt(id) == 10)$scope.image_rotate10 = "show";  
      
   var fd = new FormData();
    //Take the first selected file
    fd.append("listiclepicture", files[0]);
    writerService.storysubcontentImageUpload(fd).then(function(response) 
    {
      //alert(JSON.stringify(response));
      if(parseInt(id) == 1)
      {
        $scope.listiclearticle.subimage1 = response.imageid;
        $scope.listiclearticle.subimageurl1 = response.imageurl;
        $scope.image1 = "show";
        $scope.image_rotate1 = '';
      }
      if(parseInt(id) == 2)
      {
        $scope.listiclearticle.subimage2 = response.imageid;
        $scope.listiclearticle.subimageurl2 = response.imageurl;
        $scope.image2 = "show";
        $scope.image_rotate2 = '';
      }
      if(parseInt(id) == 3)
      {
        $scope.listiclearticle.subimage3 = response.imageid;
        $scope.listiclearticle.subimageurl3 = response.imageurl;
        $scope.image3 = "show";
        $scope.image_rotate3 = '';
      }
      if(parseInt(id) == 4)
      {
        $scope.listiclearticle.subimage4 = response.imageid;
        $scope.listiclearticle.subimageurl4 = response.imageurl;
        $scope.image4= "show";
        $scope.image_rotate4 = '';
      }
      if(parseInt(id) == 5)
      {
        $scope.listiclearticle.subimage5 = response.imageid;
        $scope.listiclearticle.subimageurl5 = response.imageurl;
        $scope.image5 = "show";
        $scope.image_rotate5 = '';
      }
      if(parseInt(id) == 6)
      {
        $scope.listiclearticle.subimage6 = response.imageid;
        $scope.listiclearticle.subimageurl6 = response.imageurl;
        $scope.image6 = "show";
        $scope.image_rotate6 = '';
      }
      if(parseInt(id) == 7)
      {
        $scope.listiclearticle.subimage7 = response.imageid;
        $scope.listiclearticle.subimageurl7 = response.imageurl;
        $scope.image7 = "show";
        $scope.image_rotate7 = '';
      }
      if(parseInt(id) == 8)
      {
        $scope.listiclearticle.subimage8 = response.imageid;
        $scope.listiclearticle.subimageurl8 = response.imageurl;
        $scope.image8 = "show";
        $scope.image_rotate8 = '';
      }
      if(parseInt(id) == 9)
      {
        $scope.listiclearticle.subimage9 = response.imageid;
        $scope.listiclearticle.subimageurl9 = response.imageurl;
        $scope.image9 = "show";
        $scope.image_rotate9 = '';
      }
      if(parseInt(id) == 10)
      {
        $scope.listiclearticle.subimage10 = response.imageid;
        $scope.listiclearticle.subimageurl10 = response.imageurl;    
        $scope.image10 = "show";  
        $scope.image_rotate10 = '';
      }
    }); 
  }

  $scope.normalStoryboardFileUpload = function(files)
  {
    var fd = new FormData();
    //Take the first selected file
    fd.append("profilepicturefile", files[0]);
    writerService.normalFileUpload(fd).then(function(response) 
    {
      $scope.imageUrl = '../uploads/dummyimages/'+response.filename;  
      $scope.showImageCropModal = !$scope.showImageCropModal;
      var el = document.getElementById('imagecrop-basic');
      $(el).html('');
      imageCroppie = new Croppie(el, {
      viewport: { width: 150, height: 200 },
      boundary: { width: 300, height: 300 },
      //showZoomer: false,
      //enableOrientation: true
      });
      ///alert($scope.imageUrl);
      imageCroppie.bind({
      url: $scope.imageUrl,
      orientation: 4
      });
    }); 
  }

  $scope.submitArticletoContentful = function()
  {
    var articledata = {"articleid":"4"};
    writerService.submitArticletoContentful(articledata).then(function(response)
    {
      //alert("Article submited to contentful successfully");
      window.location.href="/storyview";

    });
  }
  $scope.profileform = {};

  $scope.profileformSubmit = function()
  {
    $scope.profileform.imageid = $scope.imageid;
    var firstname = $scope.profileform.username;
    var lastname = $scope.profileform.lastname;
    var city = $scope.profileform.city;
    var facebooklinks = $scope.profileform.facebooklinks;
    var twitterlinks = $scope.profileform.twitterlinks;
    var email = $scope.profileform.email;
    var paypalemail = $scope.profileform.paypalemail;
    var interestNews = $scope.profileform.interestnews;
    var interestsports= $scope.profileform.interestsports;
    var interestEntertainment = $scope.profileform.interestentertainment;

    $scope.profileform.usernameerr =  "";
    $scope.profileform.lastnameerr =  "";
    $scope.profileform.cityerr =  "";
    $scope.profileform.facebooklinkserr =  "";
    $scope.profileform.twitterlinkserr =  "";
    $scope.profileform.emailerr = "";
    $scope.profileform.paypalemailerr = "";

    var facebookurl = facebooklinks.match(/https?\:\/\/(?:www\.)?facebook\.com\/(\d+|[A-Za-z0-9\.]+)\/?/);
    var twitterurl = twitterlinks.match(/https?\:\/\/(?:www\.)?twitter\.com\/(\d+|[A-Za-z0-9\.]+)\/?/);

    if(firstname != '' && lastname != '' && city != '' && email != '')
    {
      //alert(facebookurl+", "+twitterurl);
      if((facebookurl == null && facebooklinks != ""  ) || ( twitterurl == null && twitterlinks != "") || email == '')
      {
        if(facebookurl == null && facebooklinks != "")$scope.profileform.facebooklinkserr = "form-group has-danger";
        if(twitterurl == null && twitterlinks != "" )$scope.profileform.twitterlinkserr = "form-group has-danger";
        if(email == '')$scope.profileform.emailerr = "form-group has-danger";
        return;
      }      

      writerService.profileformSubmit($scope.profileform).then(function(response)
      {
        window.location.href="/storyview";
      }); 

      /*if(interestsports != undefined && interestsports == true || interestNews != undefined && interestNews == true || interestEntertainment != undefined && interestEntertainment == true)
      {
        writerService.profileformSubmit($scope.profileform).then(function(response)
        {
          window.location.href="/storyview";
        }); 
      }
      else
      {
        alert("Please select any one of intetests");
        return;
      }
      */

     
    }

    else
    {
      if(firstname == "") $scope.profileform.usernameerr = "form-group has-danger";
      if(lastname == "") $scope.profileform.lastnameerr = "form-group has-danger";
      if(city == "") $scope.profileform.cityerr = "form-group has-danger";
      if(email == '')$scope.profileform.emailerr = "form-group has-danger";
      return;
    }     
  }

  $scope.deleteArticle = function(articleid)
  {
    writerService.deleteArticle(articleid).then(function(response)
    {
      window.location.href="/storyview";
    });   
  }

  $scope.updateResultImage = function() 
  {   

      size = { width: 200, height: 200 };
      //on button click
      imageCroppie.result({type: 'canvas',size: size}).then(function(base64Image) {
        // do something with cropped base64 image here
        writerService.saveFile(base64Image).then(function(response) 
        {
          $scope.imageid = response.fileid;
          $scope.filename = response.filename;
          $scope.picturename = '../cropImgUploads/'+response.filename;
          $scope.showImageCropModal = !$scope.showImageCropModal;    
        });  
      });  
    }
}]);

unipaperapp.directive('autoSaveForm', function($timeout) {

  return {
    require: ['^form'],
    link: function($scope, $element, $attrs, $ctrls) {

      var $formCtrl = $ctrls[0];
      var savePromise = null;
      var expression = $attrs.autoSaveForm || 'true';

      $scope.$watch(function() {

        if($formCtrl.$valid && $formCtrl.$dirty) {

          if(savePromise) {
            $timeout.cancel(savePromise);
          }

          savePromise = $timeout(function() {

            savePromise = null;

            // Still valid?

            if($formCtrl.$valid) {

              if($scope.$eval(expression) !== false) {
                console.log('Form data persisted -- setting prestine flag');
                $formCtrl.$setPristine();  
              }

            }

          }, 500);
        }

      });
    }
  };

});

unipaperapp.directive('imagecropmodal', function () {
    return {
      template: '<div class="modal fade">' + 
          '<div class="modal-dialog">' + 
            '<div class="modal-content">' + 
              '<div class="modal-header">' + 
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
                '<h4 class="modal-title">{{ croppietitle }} </h4>' + 
              '</div>' + 
              '<div class="modal-body">'+
              '<div id="imagecrop-basic"></div>'+
              '<button style="margin-bottom: 5%;margin-left: 40%;" id="basic-result" class="btn btn-primary" type="button" ng-click="updateResultImage()">Upload</button>'+            
              '</div>' + 
            '</div>' + 
          '</div>' + 
        '</div>',
      restrict: 'E',
      transclude: true,
      replace:true,
      scope:true,
      link: function postLink(scope, element, attrs) 
      {
          scope.$watch(attrs.visible, function(value)
          {
            if(value == true)
            {
              $(element).find('.imgCropper-image').attr('src',scope.imageUrl);
              $(element).modal('show');
            }
            else
              $(element).modal('hide');
          });

          $(element).on('shown.bs.modal', function(){
            scope.$apply(function(){
              scope.$parent[attrs.visible] = true;
            });
          });

          $(element).on('hidden.bs.modal', function()
          {
            scope.$apply(function(){
              scope.$parent[attrs.visible] = false;
            });
          });
        }
    };
  });




unipaperapp.directive('modal', function () 
{
    return {
      template: '<div class="modal fade">' + 
          '<div class="modal-dialog">' + 
            '<div class="modal-content">'+
            '<div class="modal-header">'+
            '<button data-dismiss="modal" class="close" type="button">×</button>'+
            '<h4 class="modal-title">Welcome! <br> Thanks for registering with University Paper</h4>'+
            '</div>'+
            '<div class="modal-body">'+
            '<p>You have not completed your profile. <br> Update your profile to get started</p>'+
            '</div>'+
            '<div class="modal-footer btn-primarys">'+
            '<button data-dismiss="modal" class="btn btn-primary" type="button" ng-click="profileview()">Update profile</button>'+            
            '</div>'+
            '</div>'+
          '</div>' + 
        '</div>',
      restrict: 'E',
      transclude: true,
      replace:true,
      scope:true,
      link: function postLink(scope, element, attrs) {
          scope.$watch(attrs.visible, function(value){
          if(value == true)
            $(element).modal('show');
          else
            $(element).modal('hide');
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
});

unipaperapp.directive('uploadfile', function () {
    return {
      restrict: 'A',
      link: function(scope, element) {
        element.bind('click', function(e) {
            //angular.element(e.target).siblings('#upload').trigger('click');
            angular.element(e.target).next('input[type="file"]').trigger('click');
        });
      }
    };
});

unipaperapp.directive('errSrc', function() {
  return {
  link: function(scope, element, attrs) {
  element.bind('error', function() {
  if (attrs.src != attrs.errSrc) {
  attrs.$set('src', attrs.errSrc);
  }
  });
  }
  }
});
