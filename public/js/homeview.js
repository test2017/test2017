
var appUnipaper = angular.module("uniPaper", ["ngRoute"]);

var $httpdService = null;

appUnipaper.config(function($routeProvider, $httpProvider) 
{
	$routeProvider
	.when("/", {
		//templateUrl : "templates/loginform.htm",
      	controller: 'signinCtrl'
	})
	//.when("/signup", {
	//	templateUrl : "templates/signupform.htm",
    //  	controller: 'signupCtrl'
	//})
});

appUnipaper.controller('signinCtrl', ['$scope', '$rootScope','authencationService', function($scope, $rootScope,authencationService)
{
	// create a blank object to handle form data.
	$scope.loginform = {};
	$scope.signupform = function() {
	window.location.href= "#/signup";
	}

	$scope.submitLoginForm = function()
	{
		authencationService.signinForm($scope.loginform).then(function(response) 
		{
			if(response.code == 1)
			{
				window.location.href = "/storyview";
			}
			$scope.loginform = {};
			$scope.loginform = null;
		});
	}

	//$scope.register = function(social) 
	//{	
	//	lock.show();
	//}
}]);

appUnipaper.run(["$http", function($http) {
  // access $http and routeSegmentProvider here
  $httpdService = $http;
}]);

/*appUnipaper.controller('signupCtrl', ['$scope', '$rootScope','authencationService', function($scope, $rootScope,authencationService)
{
  	// create a blank object to handle form data.
  	$scope.signinform = {};
	$scope.loginform = function() {
	window.location.href= "/";
	}

	$scope.submitSignupForm = function()
	{  	
		authencationService.saveSignupForm($scope.signinform).then(function(response) 
		{
			if(response.code == 1){
			window.location.href = "/storyview";
		}
		$scope.signinform = {};
		$scope.signinform = null;
		});
	}

}]);*/

var options = {
    container: 'hiw-login-container',
	theme: {
		logo: 'http://unipaper-writers.herokuapp.com/img/unipaperlogo.png',
		primaryColor: 'DodgerBlue',
		popupOptions: { width: 500, height: 400, left: 200, top: 300 }
	}  ,
	languageDictionary: {
       title: ""
  },
};


// Initiating our Auth0Lock
var lock = new Auth0Lock('9V74nXrFX6yN7klTGOlXmoqZMrvasm0H','unipaper-writers-portal.auth0.com',options);

// Listening for the authenticated event
lock.on("authenticated", function(authResult) 
{
	// Use the token in authResult to getProfile() and save it to localStorage
	lock.getProfile(authResult.idToken, function(error, profile) 
	{
		if (error) {
			// Handle error
			return;
		}   
		else
		{
			var userid = profile.user_id.split('|')[1]; 
			var authtype = profile.user_id.split('|')[0]; //auth0|facebook|twitter
			var name = profile.name;
			var email = profile.email;
			var largepicture = profile.picture_large;
			var smallpicture = profile.picture;
			var email_verified = profile.email_verified;

			//if(email_verified)
			//{
				if(authtype == "auth0")
				{
					name = profile.nickname;
				}
				console.log(JSON.stringify(profile));
				var data = {
					username: name,
					userid: userid,
					authtype:authtype,
					email:email,
					largepicture:largepicture,
					smallpicture:smallpicture
				};
				        
				var config = {
				headers : {'Content-Type': 'application/json'}
				}
	            $httpdService.post('/sigininSocialAction', data, config)
	            .success(function (data, status, headers, config) {
					if(data.code)
					{
						window.location.href = "/storyview";
					}
					else
					{
						alert("User not created in server");
					}				
				})
	            .error(function (data, status, header, config) {
	                console.log("Updation error");
	            });
            //}
            //else
        	//{
    		//	alert("Email Not verified");
        	//}
		}
	});
});

lock.show();

