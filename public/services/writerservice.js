unipaperapp.service('writerService', function($http, $q)
{
  return {
    //User Registration form submit service
    'saveStory': function(formdata) {
      var defer = $q.defer();
      // Posting data to php file
      $http({
        method  : 'POST',
        url     : '/writerstory',
        data    : formdata, //forms user object
        headers : {'Content-Type': 'application/json'}
       })
       .success(function(resp){
         defer.resolve(resp);
       })
       .error( function(err) {
         defer.reject(err);
       });
       return defer.promise;
    },

    'saveListicleStory':function(formdata)
    {
       var defer = $q.defer();
      // Posting data to php file
      $http({
        method  : 'POST',
        url     : '/saveListicleStory',
        data    : formdata, //forms user object
        headers : {'Content-Type': 'application/json'}
       })
       .success(function(resp){
         defer.resolve(resp);
       })
       .error( function(err) {
         defer.reject(err);
       });
       return defer.promise;
     },

     'saveGalleryStory':function(formdata)
    {
       var defer = $q.defer();
      // Posting data to php file
      $http({
        method  : 'POST',
        url     : '/saveGalleryStory',
        data    : formdata, //forms user object
        headers : {'Content-Type': 'application/json'}
       })
       .success(function(resp){
         defer.resolve(resp);
       })
       .error( function(err) {
         defer.reject(err);
       });
       return defer.promise;
     },

    //User Registration form submit service
    'saveFile': function(imageData) {
      var defer = $q.defer();
      var uploadUrl = "/saveProfilePicture";
      var dataToSubmit = {__ContentType : "image/jpeg", base64 : imageData};
       $http({
        method  : 'POST',
        url     : uploadUrl,
        data    : dataToSubmit, //forms user object
        headers : {'Content-Type': 'application/json'}
       })
       .success(function(resp){
         defer.resolve(resp);
       })
       .error( function(err) {
         defer.reject(err);
       });
       return defer.promise;     
    },

    'storysubcontentImageUpload': function(fd) 
    {
        var defer = $q.defer();
        var uploadUrl = "/storysubcontentImageUpload"

        $http.post(uploadUrl, fd, {
          withCredentials: true,
          headers: {'Content-Type': undefined },
          transformRequest: angular.identity
        })
        .success(function(resp){
          defer.resolve(resp);
        })
        .error( function(err) {
          defer.reject(err);
        });
        return defer.promise;   
    },

    'normalFileUpload': function(fd) {
      var defer = $q.defer();
      var uploadUrl = "/normalFileUpload"

      $http.post(uploadUrl, fd, {
        withCredentials: true,
        headers: {'Content-Type': undefined },
        transformRequest: angular.identity
      })
      .success(function(resp){
        defer.resolve(resp);
      })
      .error( function(err) {
        defer.reject(err);
      });
      return defer.promise;     
    },

     'getStoryList':function(formdata) {
      var defer = $q.defer();
      // Posting data to php file
      $http({
        method  : 'get',
        url     : '/getAllStories',
        headers : {'Content-Type': 'application/json'}
       })
       .success(function(resp){
         defer.resolve(resp);
       })
       .error( function(err) {
         defer.reject(err);
       });
       return defer.promise;
    },

    'getNotifications':function()
    {
      var defer = $q.defer();
      // Posting data to php file
      $http({
        method  : 'get',
        url     : '/getNotifications',
        headers : {'Content-Type': 'application/json'}
       })
       .success(function(resp){
         defer.resolve(resp);
       })
       .error( function(err) {
         defer.reject(err);
       });
       return defer.promise;
    },

    'updateNotification':function(id)
    {
        var defer = $q.defer();
        // Posting data to php file
        $http({
        method  : 'POST',
        url     : '/updateNotification',
        data    : {'notifyid':id}, //forms user object
        headers : {'Content-Type': 'application/json'}
        })
        .success(function(resp){
        defer.resolve(resp);
        })
        .error( function(err) {
        defer.reject(err);
        });
        return defer.promise;
    },

    'deleteNotification':function(id)
    {
       var defer = $q.defer();
        // Posting data to php file
        $http({
        method  : 'POST',
        url     : '/deleteNotification',
        data    : {'notifyid':id}, //forms user object
        headers : {'Content-Type': 'application/json'}
        })
        .success(function(resp){
        defer.resolve(resp);
        })
        .error( function(err) {
        defer.reject(err);
        });
        return defer.promise;
    },

    'submitArticletoContentful': function(formdata) {
      var defer = $q.defer();
      // Posting data to php file
      $http({
        method  : 'POST',
        url     : '/submitArticletoContentful',
        data    : formdata, //forms user object
        headers : {'Content-Type': 'application/json'}
       })
       .success(function(resp){
         defer.resolve(resp);
       })
       .error( function(err) {
         defer.reject(err);
       });
       return defer.promise;
    },
    'getProfileupdateStatus':function(formdata) {
      var defer = $q.defer();
      // Posting data to php file
      $http({
        method  : 'get',
        url     : '/getProfileupdateStatus?process='+formdata,
        headers : {'Content-Type': 'application/json'}
       })
       .success(function(resp){
         defer.resolve(resp);
       })
       .error( function(err) {
         defer.reject(err);
       });
       return defer.promise;
    },

    'profileformSubmit': function(formdata) 
    {
      var defer = $q.defer();
      // Posting data to php file
      $http({
        method  : 'POST',
        url     : '/profileformSubmit',
        data    : formdata, //forms user object
        headers : {'Content-Type': 'application/json'}
       })
       .success(function(resp)
       {
         defer.resolve(resp);
       })
       .error( function(err) 
       {
         defer.reject(err);
       });
       return defer.promise;
    },
    'updateCategory': function(formdata) 
    {
      var defer = $q.defer();
      // Posting data to php file
      $http({
        method  : 'POST',
        url     : '/updateCategory',
        data    : {"category":formdata}, //forms user object
        headers : {'Content-Type': 'application/json'}
       })
       .success(function(resp)
       {
         defer.resolve(resp);
       })
       .error( function(err) 
       {
         defer.reject(err);
       });
       return defer.promise;
    },
    'getSearchStories':function(searchtext)
    {
      var defer = $q.defer();
      // Posting data to php file
      $http({
        method  : 'POST',
        url     : '/getSearchStories',
        data    : {"searchtext":searchtext}, //forms user object
        headers : {'Content-Type': 'application/json'}
       })
       .success(function(resp)
       {
         defer.resolve(resp);
       })
       .error( function(err) 
       {
         defer.reject(err);
       });
       return defer.promise; 
    },

    'deleteArticle':function(articleid)
     {
      var defer = $q.defer();
      // Posting data to php file
      $http({
        method  : 'POST',
        url     : '/deleteArticle',
        data    : {"articleid":articleid}, //forms user object
        headers : {'Content-Type': 'application/json'}
       })
       .success(function(resp)
       {
         defer.resolve(resp);
       })
       .error( function(err) 
       {
         defer.reject(err);
       });
       return defer.promise; 
    },

    
    'getCityNames':function(formdata) {
      var defer = $q.defer();
      // Posting data to php file
      $http({
        method  : 'get',
        url     : '/getCityNames',
        headers : {'Content-Type': 'application/json'}
       })
       .success(function(resp){
         defer.resolve(resp);
       })
       .error( function(err) {
         defer.reject(err);
       });
       return defer.promise;
    },

    'getStory':function(formdata) {
      var defer = $q.defer();
      // Posting data to php file
      $http({
        method  : 'get',
        url     : '/getStory',
        headers : {'Content-Type': 'application/json'}
       })
       .success(function(resp){
         defer.resolve(resp);
       })
       .error( function(err) {
         defer.reject(err);
       });
       return defer.promise;
    },

}});

