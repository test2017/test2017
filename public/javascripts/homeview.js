
var appUnipaper = angular.module("uniPaper", ["ngRoute",'auth0', 'angular-storage', 'angular-jwt']);

//OAuth.initialize('LBntk8TZAhSwyEFcqSeX0UtUVg8');
var $httpdService = null;


appUnipaper.config(function($routeProvider, authProvider, $httpProvider, $locationProvider, jwtInterceptorProvider) {
	$routeProvider
		.when("/", {
			templateUrl : "templates/loginform.htm",
          	controller: 'signinCtrl'
		})
		.when("/signup", {
			templateUrl : "templates/signupform.htm",
          	controller: 'signupCtrl'
		})

		authProvider.init({
	    domain: AUTH0_DOMAIN,
	    clientID: AUTH0_CLIENT_ID,
	    loginUrl: '/login'
	  });

  	//Called when login is successful
  	authProvider.on('loginSuccess', function($location, profilePromise, idToken, store) {
	    console.log("Login Success");
	    profilePromise.then(function(profile) {
	      store.set('profile', profile);
	      store.set('token', idToken);
	      //alert("userid, username = "+profile.user_id.split('|')[1]+", "+profile.name);
	      var userid = profile.user_id.split('|')[1];
			var name = profile.name;
			/*
			authencationService.signinSocialForm(userid,name).then(function(response) 
			{
				if(response.code)
				{
					window.location.href = "/storyview";
				}
				else
				{
					alert("User not created in server");
				}
			});
			*/

			var data = {username: name,userid: userid};
        
            var config = {
    			headers : {'Content-Type': 'application/json'}
            }

            $httpdService.post('/sigininSocialAction', data, config)
            .success(function (data, status, headers, config) {
				if(data.code)
				{
					window.location.href = "/storyview";
				}
				else
				{
					alert("User not created in server");
				}				
			})
            .error(function (data, status, header, config) {
                alert("Updation error")
            });
	    });
	    //$location.path('/');



	  });

	  authProvider.on('loginFailure', function() {
	    alert("Error");
	  });

	  authProvider.on('authenticated', function($location) {
	    console.log("Authenticated");

	  });

	  jwtInterceptorProvider.tokenGetter = function(store) {
	    return store.get('token');
	  };

	  // Add a simple interceptor that will fetch all requests and add the jwt token to its authorization header.
	  // NOTE: in case you are calling APIs which expect a token signed with a different secret, you might
	  // want to check the delegation-token example
	  $httpProvider.interceptors.push('jwtInterceptor');
});

appUnipaper.controller('signinCtrl', ['$scope', '$rootScope','authencationService','auth', function($scope, $rootScope,authencationService,auth)
{
	// create a blank object to handle form data.
	$scope.loginform = {};
	$scope.signupform = function() {
	window.location.href= "#/signup";
	}

	$scope.submitLoginForm = function()
	{
		authencationService.signinForm($scope.loginform).then(function(response) 
		{
			if(response.code == 1)
			{
				window.location.href = "/storyview";
			}
			$scope.loginform = {};
			$scope.loginform = null;
		});
	}

	$scope.register = function(social) 
	{
		/*
		OAuth.popup(social).then(function(result) 
		{
			return result.me();
		}).done(function(me) {
			var userid = me.id;
			var name = me.name;
			authencationService.signinSocialForm(userid,name).then(function(response) 
			{
				if(response.code)
				{
					window.location.href = "/storyview";
				}
				else
				{
					alert("User not created in server");
				}
			});

		}).fail(function(err) {
		console.log(err);
		});
		*/
	    auth.signin()

	}

	

}]);

appUnipaper.run(["$http", function($http) {
  // access $http and routeSegmentProvider here
  $httpdService = $http;
}]);

appUnipaper.run(['auth', function(auth) {
  // This hooks all auth events to check everything as soon as the app starts
  auth.hookEvents();
}]);

appUnipaper.run(function($rootScope, auth, store, jwtHelper, $location) {
  $rootScope.$on('$locationChangeStart', function() {

    var token = store.get('token');
    if (token) {
      if (!jwtHelper.isTokenExpired(token)) {
        if (!auth.isAuthenticated) {
          //Re-authenticate user if token is valid
          auth.authenticate(store.get('profile'), token);
        }
      } else {
        // Either show the login page or use the refresh token to get a new idToken

        $location.path('/');
      }
    }
  });
});

appUnipaper.controller('signupCtrl', ['$scope', '$rootScope','authencationService','auth', function($scope, $rootScope,authencationService,auth)
{
  // create a blank object to handle form data.
  $scope.signinform = {};
	$scope.auth = auth;

  $scope.loginform = function() {
  	window.location.href= "/";
  }

  $scope.submitSignupForm = function()
  {  	
 	authencationService.saveSignupForm($scope.signinform).then(function(response) 
 	{
      //alert(response.msg);
      if(response.code == 1){
      	window.location.href = "/storyview";
      }
      $scope.signinform = {};
      $scope.signinform = null;
    });
  }

}]);

